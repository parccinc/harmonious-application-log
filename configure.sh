#!/bin/bash

# HAL configuration setup
# Run as root
# Usage bash configure.sh owner_group=group owner_user=user dbhost=host dbname=name dbuser=user dbpwd=pwd max_datetime_delta=123 record_performance=true token_timeout=123 requestscheme=scheme appdir=dir client_user=username client_pwd=password log=false
# owner_group is group that owns directory where application is deployed (ex.: apache)
# owner_user is group that owns directory where application is deployed (ex.: apache)
# dbhost is HAL database host. If not specified, script will prompt for database host.
# dbname is HAL database name. If not specified, script will prompt for database name.
# dbuser is HAL database user. If not specified, script will prompt for database user.
# dbpwd is HAL database user password. If not specified, script will prompt for password.
# max_datetime_delta used in hal_svcs/config.php. If not specified, script will prompt for it.
# record_performance used in hal_svcs/config.php. If not specified, script will prompt for it.
# token_timeout used in hal_svcs/config.php. If not specified, script will prompt for it.
# requestscheme is HAL client request scheme (http or https. If not specified, script will prompt for scheme.
# appdir is directory on Apache server where code is deployed.
# client_user is HAL client username.
# client_pwd is HAL client password.
# log enables this script log file (true or false). Default is false.

USELOG=false
LOGFILE=configure.sh.log
echo `date` >$LOGFILE
OWNER_GROUP=
OWNER_USER=
DB_DISABLE_TABLE_OPERATIONS=
DBHOST=
DBNAME=
DBUSER=
DBPWD=
MAX_DATETIME_DELTA=
RECORD_PERFORMANCE=
TOKEN_TIMEOUT=
REQUESTSCHEME=
APPDIR=
HALCLIENTUSER=
HALCLIENTPWD=

# Process script parameters
if [ $# -gt 0 ];then
	for (( i=1; i<=$#; i++ ));do
		IFS='=' read -ra ARR <<< "${@:i:1}"
		case "${ARR[0]}" in
			owner_group)
				OWNER_GROUP="${ARR[1]}"
				;;
				
			owner_user)
				OWNER_USER="${ARR[1]}"
				;;
			
		        DB_DISABLE_TABLE_OPERATIONS)
			    DB_DISABLE_TABLE_OPERATIONS="${ARR[1]}"
			    ;;
			
			dbhost)
				DBHOST="${ARR[1]}"
				;;
				
			dbname)
				DBNAME="${ARR[1]}"
				;;
			
			dbuser)
				DBUSER="${ARR[1]}"
				;;
				
			dbpwd)
				DBPWD="${ARR[1]}"
				;;
				
			max_datetime_delta)
				MAX_DATETIME_DELTA="${ARR[1]}"
				;;
				
			record_performance)
				RECORD_PERFORMANCE="${ARR[1]}"
				;;
				
			token_timeout)
				TOKEN_TIMEOUT="${ARR[1]}"
				;;
				
			requestscheme)
				REQUESTSCHEME="${ARR[1]}"
				;;
				
			appdir)
				APPDIR="${ARR[1]}"
				;;
				
			client_user)
				HALCLIENTUSER="${ARR[1]}"
				;;
				
			client_pwd)
				HALCLIENTPWD="${ARR[1]}"
				;;
				
			log)
				USELOG="${ARR[1]}"
				;;
		esac
	done
fi

log() {
	COLOR=
	if [ $# -gt 1 ];then
		case "$2" in
			error)
				COLOR="31"
				;;

			warn)
				COLOR="33"
				;;
					
			ok)
				COLOR="32"
				;;
		esac
	fi

	local MSG=
	if [ -z "$COLOR" ];then
		MSG="$1"
	else
		MSG="\\E[0;""$COLOR""m""$1""\\E[0m"
	fi

	if [ "$USELOG" = true ]; then
		echo -e "$MSG" | tee -a "$LOGFILE"
	else
		echo -e "$MSG"
	fi
}

# Check for required input.
check_required() {
	if [ -z "$1" ];then
		while true;do
			read -p "$2"": "
			if [ ! -z "$REPLY" ];then
				if [ $# -gt 2 ];then
					local MATCH=false
					for (( i=3; i<=$#; i++ ));do
						if [ "${@:i:1}" == "$REPLY" ];then
							MATCH=true;
							break;
						fi
					done
					
					if [ "$MATCH" = true ];then
						echo "$REPLY"
						break
					fi
				else
					echo "$REPLY"
					break
				fi
			fi
		done
	else
		echo $1
	fi
}

# Executed command and logs
cmd() {
	log "$1"
	local ERR=
	if [ "$USELOG" = true ];then
		local ERR=$((eval $1 >>$LOGFILE) 2>&1)
		if [ $? -gt 0 ];then
			log "$ERR" error
			exit 1
		fi
	else
		eval $1 2>&1
	fi
	
}

# Gets Status from HTTP response.
status() {
	local STATUS=$(echo "$1" | awk '/^  HTTP/{print $2}')
	echo "${STATUS: -3}"
}

OWNER_GROUP=$(check_required "$OWNER_GROUP" "Application directory group owner")
OWNER_USER=$(check_required "$OWNER_USER" "Application directory user owner")
DBHOST=$(check_required "$DBHOST" "HAL database host")
DBNAME=$(check_required "$DBNAME" "HAL database name")
DBUSER=$(check_required "$DBUSER" "HAL database user")
if [ -z "$DBPWD" ];then
	while true;do
		read -sp "HAL Database password: " PWD
		echo
		if [ ! -z "$PWD" ];then
			read -sp "HAL Database password (again): " AGAIN
			echo
			if [ "$PWD" = "$AGAIN" ];then
				DBPWD="$PWD"
				break
			else
				echo "Please try again"
			fi
		fi
	done
fi

MAX_DATETIME_DELTA=$(check_required "$MAX_DATETIME_DELTA" "HAL max_datetime_delta")
RECORD_PERFORMANCE=$(check_required "$RECORD_PERFORMANCE" "HAL record_performance")
TOKEN_TIMEOUT=$(check_required "$TOKEN_TIMEOUT" "HAL token_timeout")
OUTCOMES=("http" "https")
REQUESTSCHEME=$(check_required "$REQUESTSCHEME" "HAL client request scheme [http|https]" ${OUTCOMES[@]})
HALCLIENTUSER=$(check_required "$HALCLIENTUSER" "HAL client username")
HALCLIENTPWD=$(check_required "$HALCLIENTPWD" "HAL client password")

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cmd "cp '$DIR/hal_svcs/default.config.php' '$DIR/hal_svcs/config.php'"
cmd "sed -i 's/define('\\''DB_HOSTNAME'\\''\\([[:space:]]*\\),\\([[:space:]]*\\)\"\");/define('\\''DB_HOSTNAME'\\''\\1,\\2\"$DBHOST\");/g' '$DIR/hal_svcs/config.php'"
cmd "sed -i 's/define('\''DB_DISABLE_TABLE_OPERATIONS'\'',.*);/define('\''DB_DISABLE_TABLE_OPERATIONS'\'', '"${DB_DISABLE_TABLE_OPERATIONS}"');/g' '$DIR/hal_svcs/config.php'"
cmd "sed -i 's/define('\\''DB_NAME'\\''\\([[:space:]]*\\),\\([[:space:]]*\\)\"\");/define('\\''DB_NAME'\\''\\1,\\2\"$DBNAME\");/g' '$DIR/hal_svcs/config.php'"
cmd "sed -i 's/define('\\''DB_USERNAME'\\''\\([[:space:]]*\\),\\([[:space:]]*\\)\"\");/define('\''DB_USERNAME'\''\\1,\\2\"$DBUSER\");/g' '$DIR/hal_svcs/config.php'"
cmd "sed -i 's/define('\\''DB_PASSWORD'\\''\\([[:space:]]*\\),\\([[:space:]]*\\)\"\");/define('\\''DB_PASSWORD'\\''\\1,\\2\"$DBPWD\");/g' '$DIR/hal_svcs/config.php'"
cmd "sed -i 's/define('\\''MAX_DATETIME_DELTA'\\''\\([[:space:]]*\\),\\([[:space:]]*\\)\\([0-9]*\\));.*/define('\\''MAX_DATETIME_DELTA'\\''\\1,\\2$MAX_DATETIME_DELTA);/g' '$DIR/hal_svcs/config.php'"
cmd "sed -i 's/define('\\''RECORD_PERFORMANCE'\\''\\([[:space:]]*\\),\\([[:space:]]*\\)\\(.*\\));/define('\\''RECORD_PERFORMANCE'\\''\\1,\\2$RECORD_PERFORMANCE);/g' '$DIR/hal_svcs/config.php'"
cmd "sed -i 's/define('\\''TOKEN_TIMEOUT'\\''\\([[:space:]]*\\),\\([[:space:]]*\\)\\([0-9]*\\));.*/define('\\''TOKEN_TIMEOUT'\\''\\1,\\2$TOKEN_TIMEOUT);/g' '$DIR/hal_svcs/config.php'"
cmd "cp '$DIR/hal_client/default.config.php' '$DIR/hal_client/config.php'"
cmd "sed -i 's/define('\\''REQUEST_SCHEME'\\''\\([[:space:]]*\\),\\([[:space:]]*\\)\"https\");/define('\''REQUEST_SCHEME'\''\\1,\\2\"$REQUESTSCHEME\");/g' '$DIR/hal_client/config.php'"
if [ -z "$APPDIR" ];then
	cmd "sed -i 's/define('\\''HAL_RESOURCE_PATH'\\''\\([[:space:]]*\\),\\([[:space:]]*\\)\"\/PARCC-HAL\/hal_svcs\/\");/define('\''HAL_RESOURCE_PATH'\''\\1,\\2\"\/hal_svcs\/\");/g' '$DIR/hal_client/config.php'"
else
	cmd "sed -i 's/define('\\''HAL_RESOURCE_PATH'\\''\\([[:space:]]*\\),\\([[:space:]]*\\)\"\/PARCC-HAL\/hal_svcs\/\");/define('\''HAL_RESOURCE_PATH'\''\\1,\\2\"\/$APPDIR\/hal_svcs\/\");/g' '$DIR/hal_client/config.php'"
fi

cmd "chown -R ""$OWNER_GROUP"":""$OWNER_USER"" -R '$DIR'"

# Create HAL tables
REQUEST="wget --server-response --no-cache --no-cookies --no-check-certificate -qO- ""$REQUESTSCHEME""://localhost/$APPDIR/hal_client/create_all_tables.php"
log "$REQUEST"
RESPONSE=$($REQUEST 2>&1)
log "$RESPONSE"
STATUS=$(status "$RESPONSE")
if [ "$STATUS" = 200 ];then
	SUCCESS=$(echo "$RESPONSE" | grep "<p class=\"status\">STATUS: OKAY</p>")
	if [ -z "$SUCCESS" ];then
		SUCCESS=$(echo "$RESPONSE" | grep "Base table or view already exists")
		if [ -z "$SUCCESS" ];then
			log "Failed to create tables." error
			exit 1
		else
			log "Tables already exist." warn
		fi
	else
		log "Tables created." ok
	fi
else
	log "Failed to create tables." error
	exit 1
fi

# Check database version.
rm -f cookies.txt
REQUEST="wget --server-response --no-cache --no-check-certificate --save-cookies cookies.txt -qO- ""$REQUESTSCHEME""://localhost/$APPDIR/hal_client/login_user.php --post-data clientName=""$HALCLIENTUSER""&password=""$HALCLIENTPWD"
log "$REQUEST"
RESPONSE=$($REQUEST 2>&1)
log "$RESPONSE"

REQUEST="wget --server-response --no-cache --no-check-certificate --keep-session-cookies --load-cookies cookies.txt -qO- ""$REQUESTSCHEME""://localhost/$APPDIR/hal_client/get_database_version.php"
log "$REQUEST"
RESPONSE=$($REQUEST 2>&1)
log "$RESPONSE"
STATUS=$(status "$RESPONSE")
if [ "$STATUS" = 200 ];then
	SUCCESS=$(echo "$RESPONSE" | grep "<p class=\"status\">STATUS: OKAY</p>")
	if [ -z "$SUCCESS" ];then
		log "Failed to get database version." error
	else
		UPDATE=$(echo "$RESPONSE" | grep "<p class=\"status\">DATABASES: OKAY</p>")
		if [ -z "$UPDATE" ];then
			echo "Updating tables..."
			REQUEST="wget --server-response --no-cache --no-check-certificate --keep-session-cookies --load-cookies cookies.txt -qO- ""$REQUESTSCHEME""://localhost/$APPDIR/hal_client/update_database.php"
			log "$REQUEST"
			RESPONSE=$($REQUEST 2>&1)
			log "$RESPONSE"
			STATUS=$(status "$RESPONSE")
			if [ "$STATUS" = 200 ] || [ "$STATUS" = 302 ];then
				SUCCESS=$(echo "$RESPONSE" | grep "<p class=\"status\">STATUS: OKAY</p>")
				if [ -z "$SUCCESS" ];then
					log "Failed to update tables." error
					exit 1
				else
					UPDATE=$(echo "$RESPONSE" | grep "<p class=\"status\">DATABASE: OKAY</p>")
					if [ -z "$UPDATE" ];then
						log "Failed to update tables." error
					else
						log "Tables updated." ok
					fi
				fi
			else
				log "Failed to update tables." error
				exit 1
			fi
		else
			log "Tables up to date." ook
		fi
	fi
else
	log "Failed to create tables." error
	exit 1
fi

log "Complete!" ok
exit 0
