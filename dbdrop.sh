#!/bin/sh
# This script drops database and user
# Usage ./dbdrop.sh dbhost=dbhost dbname=name user=user mysqluser=user mysqlpwd=password host=host
# dbhost is database host. Default is localhost.
# dbname is database name
# user is user name part of database account
# mysqluser is user on MySQL
# mysqlpwd is password for user on MySQL
# host is host name part of database account

LOGFILE=dbdrop.sh.log
echo `date` >$LOGFILE
DBHOST=
DBNAME=
MYSQLUSER=
MYSQLPWD=
USER=
HOST=

log() {
	COLOR=
	if [ $# -gt 1 ];then
		case "$2" in
			error)
				COLOR="31"
				;;

			warn)
				COLOR="33"
				;;
					
			ok)
				COLOR="32"
				;;
		esac
	fi

	local MSG=
	if [ -z "$COLOR" ];then
		MSG="$1"
	else
		MSG="\\E[0;""$COLOR""m""$1""\\E[0m"
	fi

	echo -e "$MSG" | tee -a "$LOGFILE"
}

if [ $# -gt 0 ];then
	for (( i=1; i<=$#; i++ ));do
		IFS='=' read -ra ARR <<< "${@:i:1}"
		case "${ARR[0]}" in
			dbhost)
				DBHOST="${ARR[1]}"
				;;
				
			dbname)
				DBNAME="${ARR[1]}"
				;;

			user)
				USER="${ARR[1]}"
				;;

			mysqluser)
				MYSQLUSER="${ARR[1]}"
				;;
				
			mysqlpwd)
				MYSQLPWD="${ARR[1]}"
				;;
				
			host)
				HOST="${ARR[1]}"
				;;
		esac
	done
fi

if [ -z "$DBNAME" ];then
	log "Database name is required." error
	exit 1
fi

if [ -z "$USER" ];then
	log "Database user name is required." error
	exit 1
fi

if [ -z "$MYSQLUSER" ];then
	log "MySQL user is required." "error"
	exit 1
fi

if [ -z "$MYSQLPWD" ];then
	log "MySQL password is required." "error"
	exit 1
fi

if [ -z "$HOST" ];then
	log "HAL host is required." error
	exit 1
fi

execcmd() {
	log "$1"
	local ERR=$((eval $1 >>$LOGFILE) 2>&1)
	if [ $? -gt 0 ];then
		log "$ERR" error
		exit 1
	fi
}

dbexists() {
	local RESULT=$($MYSQL "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '$DBNAME'")
	if [ -z "$RESULT" ];then
		echo false
	else
		echo true
	fi
}

userexists() {
	local RESULT=$($MYSQL "SELECT User FROM mysql.user WHERE User='$USER' AND Host='$HOST'")
	if [ -z "$RESULT" ];then
		echo false
	else
		echo true
	fi
}

if [ -z "$DBHOST" ];then
	MYSQL="mysql -u $MYSQLUSER -p$MYSQLPWD -s -N -e"
else
	MYSQL="mysql -h $DBHOST -u $MYSQLUSER -p$MYSQLPWD -s -N -e"
fi

RESULT=$(dbexists)
if [ "$RESULT" = true ];then
	PROCESSLIST=`$MYSQL 'SELECT Id FROM INFORMATION_SCHEMA.PROCESSLIST'`
	for PROCESS in ${PROCESSLIST[@]};do
		execcmd "$MYSQL 'KILL $PROCESS'"
	done
	
	execcmd "$MYSQL 'DROP DATABASE $DBNAME'"
	RESULT=$(dbexists)
	if [ "$RESULT" = true ];then
		log "Failed to drop database $DBNAME." error
		exit 1
	else
		log "Database $DBNAME dropped!" ok
	fi
else
	log "Database $DBNAME not found. Nothing to drop!" warn
fi

RESULT=$(userexists)
if [ "$RESULT" = true ];then
	execcmd "$MYSQL 'DROP USER '\\''$USER'\\''@'\\''$HOST'\\'"
	RESULT=$(userexists)
	if [ "$RESULT" = true ];then
		log "Failed to drop user $USER@$HOST." error
		exit 1
	else
		log "Database user $USER@$HOST dropped!" ok
	fi
else
	log "Database user $USER@$HOST not found. Nothing to drop!" warn
fi

exit 0