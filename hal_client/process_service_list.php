<?php

require_once 'guzzle_client.php';

set_time_limit(0);

session_start();

define('SRC_DIR', 'process/');

// Check that the user has been authenticated
if (!isset($_SESSION['hal_token']))
{
    $error = true;
    displayHeader($error);
    displayErrorMessage('Please log in first');
    displayFooter();
    exit;
}

// Open the service list file
if (!isset($_GET['filename']))
{
    $error = true;
    displayHeader($error);
    displayErrorMessage('Please specify the process list file name - e.g. filename=some_file.json');
    displayFooter();
    exit;
}
$fileMain = SRC_DIR . $_GET['filename'];
$fhMain = fopen($fileMain, 'rb');
if ($fhMain === false)
{
    $error = true;
    displayHeader($error);
    displayErrorMessage('Unable to open file ' . $fileMain);
    displayFooter();
    exit;
}
$fileCur = $fileMain;
$fhCur = $fhMain;
$mainLine = 0;
$curLine = 0;

// Save the start time
$timeStart = time();

// Process the service list
$args = null;
$method = null;
$resource = null;
$unitTestCount = 0;
$commandsProcessed = 0;
$logEntriesCreated = 0;
$timeStampIncrement = 0;
$totalUnitTestCount = 0;
$skipProcessing = false;
$unitTests = false;
$unitTestName = '';
$jsonLine = null;
$errors = false;
$error = false;
$line = null;

$outputStart = '<p class="output">';
$output = $outputStart;
$errorExpected = false;
$actualResults = [];
$errorMessage = '';
$response = [];
$fhSub = null;
$subLine = 0;
while (true)
{
    // Read the next line
    if ($fhCur == $fhMain)
    {
        $mainLine++;
        $curLine = $mainLine;
    }
    elseif ($fhCur == $fhSub)
    {
        $subLine++;
        $curLine = $subLine;
    }
    $jsonLine = fgets($fhCur);
    if ($jsonLine === false)
    {
        // If we have reached the end of the main file we are done
        if ($fhCur == $fhMain)
            break;

        // If we have reached the end of a sub-file then close it and switch back to the main file
        if ($fhCur == $fhSub)
        {
            fclose($fhSub);
            $fhCur = $fhMain;
            $fhSub = null;
            continue;
        }
    }
    if (strlen(trim($jsonLine)) == 0) // Skip blank lines
        continue;
    if ($jsonLine[0] == '#') // Skip comment lines
        continue;

    // Decode the line
    $line = json_decode($jsonLine, true);
    if ($line === null)
    {
        $output .= $fileCur . 'line ' . $curLine . ': Unable to decode ' . $jsonLine . '</br >';
        $error = true;
        break;
    }

    // Check if this line should be skipped
    if ($skipProcessing && $line['command'] != 'SkipBlockEnd')
        continue;

    // Process the command
    $args = null;
    $method = null;
    $prefix = $fileCur . ' line ' . $curLine . ': ' . $line['command'] . ' ';
    $suffix = '<br />' . $unitTestName . ' UNIT TEST FAILED<br />';
    switch ($line['command'])
    {
        case 'CheckErrorResult':
            if (!array_key_exists('error', $line))
            {
                $output .= $prefix . ' is missing a value<br />';
                $error = true;
                break 2;
            }
            if ($actualResults['error'] != $line['error'])
            {
                $errorMessage = $prefix . 'failed<br />|error code ' . $actualResults['error'] .
                    '|<br />&nbsp;&nbsp;&nbsp;&nbsp;does not match expected<br />|error code ' .
                    $line['error'] . '|' . $suffix;
                $error = true;
                break 2;
            }
            if (array_key_exists('detail', $line))
            {
                if ($actualResults['detail'] != $line['detail'])
                {
                    $errorMessage = $prefix . 'failed<br />|' . $actualResults['detail'] .
                        '|<br />&nbsp;&nbsp;&nbsp;&nbsp;does not match expected error<br />|' .
                        $line['detail'] . '|' . $suffix;
                    $error = true;
                    break 2;
                }
            }
            $unitTestCount++;
            break;
        case 'CompareResultFile':
            if (!array_key_exists('filename', $line))
            {
                $output .= $prefix . ' is missing a value<br />';
                $error = true;
                break 2;
            }
            $jsonReference = file_get_contents(SRC_DIR . $line['filename']);
            if ($jsonReference === false)
            {
                $output .= $prefix . ' unable to load ' . $line['filename'] . $suffix;
                $error = true;
                break 2;
            }
            $expectedResults = json_decode($jsonReference, true);
            if ($expectedResults === null)
            {
                $output .= $prefix . ' unable to decode ' . $line['filename'] . $suffix;
                $error = true;
                break 2;
            }
            $diff = compareResultArrays($expectedResults, $actualResults);
            if ($diff !== null)
            {
                $errorMessage = $prefix . $diff . $suffix;
                $error = true;
                break 2;
            }
            $unitTestCount++;
            break;
        case 'CreateAllTables':
            $resource = 'tables';
            $method = 'POST';
            break;
        case 'CreateLogEntry':
            $args = [];
            if (array_key_exists('appType', $line))
                $args['appType'] = $line['appType'];
            if (array_key_exists('hostname', $line))
                $args['hostname'] = $line['hostname'];
            if (array_key_exists('severity', $line))
                $args['severity'] = $line['severity'];
            if (array_key_exists('eventType', $line))
                $args['eventType'] = $line['eventType'];
            if (array_key_exists('eventDesc', $line))
                $args['eventDesc'] = $line['eventDesc'];
            if (array_key_exists('appDateTime', $line))
                $args['appDateTime'] = $line['appDateTime'];
            else
                $args['appDateTime'] = date("Y-m-d H:i:s", time() + $timeStampIncrement);
            $resource = 'log-entry';
            $method = 'POST';
            break;
        case 'CreateNotificationRecipient':
            $args = [];
            if (array_key_exists('recipient', $line))
                $args['recipient'] = $line['recipient'];
            $resource = 'notification-recipient/';
            if (array_key_exists('notificationRuleId', $line))
                $resource .= $line['notificationRuleId'];
            $method = 'POST';
            break;
        case 'CreateNotificationRule':
            $args = [];
            if (array_key_exists('appType', $line))
                $args['appType'] = $line['appType'];
            if (array_key_exists('severity', $line))
                $args['severity'] = $line['severity'];
            if (array_key_exists('eventType', $line))
                $args['eventType'] = $line['eventType'];
            $resource = 'notification-rule';
            $method = 'POST';
            break;
        case 'CreateTable':
            if (array_key_exists('table', $line))
            {
                $resource = 'table/' . $line['table'];
                $method = 'POST';
            }
            break;
        case 'DecrementUnitTestCount':
            $unitTestCount--;
            break;
        case 'DeleteNotificationRecipient':
            $resource = 'notification-recipient/';
            if (array_key_exists('notificationListId', $line))
                $resource .= $line['notificationListId'];
            $method = 'DELETE';
            break;
        case 'DeleteNotificationRule':
            $resource = 'notification-rule/';
            if (array_key_exists('notificationRuleId', $line))
                $resource .= $line['notificationRuleId'];
            $method = 'DELETE';
            break;
        case 'DropAllTables':
            $resource = 'tables';
            $method = 'DELETE';
            break;
        case 'DropTable':
            if (array_key_exists('table', $line))
            {
                $resource = 'table/' . $line['table'];
                $method = 'DELETE';
            }
            break;
        case 'EndUnitTests':
            break 2;
        case 'GetLogEntries':
            $args = [];
            if (array_key_exists('startDateTime', $line))
                $args['startDateTime'] = $line['startDateTime'];
            if (array_key_exists('endDateTime', $line))
                $args['endDateTime'] = $line['endDateTime'];
            if (array_key_exists('appType', $line))
                $args['appType'] = $line['appType'];
            if (array_key_exists('hostname', $line))
                $args['hostname'] = $line['hostname'];
            if (array_key_exists('severity', $line))
                $args['severity'] = $line['severity'];
            if (array_key_exists('eventType1', $line))
                $args['eventType1'] = $line['eventType1'];
            if (array_key_exists('eventType2', $line))
                $args['eventType2'] = $line['eventType2'];
            if (array_key_exists('pageOffset', $line))
                $args['pageOffset'] = $line['pageOffset'];
            if (array_key_exists('pageLimit', $line))
                $args['pageLimit'] = $line['pageLimit'];
            $resource = 'log-entries';
            $method = 'GET';
            break;
        case 'GetNotificationList':
            $args = [];
            if (array_key_exists('notificationRuleId', $line))
                $args['notificationRuleId'] = $line['notificationRuleId'];
            if (array_key_exists('recipient', $line))
                $args['recipient'] = $line['recipient'];
            $resource = 'notification-list';
            $method = 'GET';
            break;
        case 'GetNotificationLogEntries':
            $args = [];
            if (array_key_exists('startDateTime', $line))
                $args['startDateTime'] = $line['startDateTime'];
            if (array_key_exists('endDateTime', $line))
                $args['endDateTime'] = $line['endDateTime'];
            if (array_key_exists('appLogId', $line))
                $args['appLogId'] = $line['appLogId'];
            if (array_key_exists('notificationRuleId', $line))
                $args['notificationRuleId'] = $line['notificationRuleId'];
            if (array_key_exists('recipient', $line))
                $args['recipient'] = $line['recipient'];
            if (array_key_exists('pageOffset', $line))
                $args['pageOffset'] = $line['pageOffset'];
            if (array_key_exists('pageLimit', $line))
                $args['pageLimit'] = $line['pageLimit'];
            $resource = 'notification-log-entries';
            $method = 'GET';
            break;
        case 'GetNotificationRules':
            $args = [];
            if (array_key_exists('appType', $line))
                $args['appType'] = $line['appType'];
            if (array_key_exists('severity', $line))
                $args['severity'] = $line['severity'];
            if (array_key_exists('eventType', $line))
                $args['eventType'] = $line['eventType'];
            $resource = 'notification-rules';
            $method = 'GET';
            break;
        case 'IncrementTimestamp':
            if (array_key_exists('seconds', $line))
                $timeStampIncrement += $line['seconds'];
            continue 2;
        case 'IncrementUnitTestCount':
            $unitTestCount++;
            break;
        case 'PauseProcessing':
            $seconds = 2;
            if (array_key_exists('seconds', $line))
                $seconds = $line['seconds'];
            sleep($seconds);
            continue 2;
        case 'ReportUnitTestPassed':
            $output .= $unitTestName . ' unit tests passed ('. $unitTestCount . ' unit tests)<br />';
            $totalUnitTestCount += $unitTestCount;
            $error = $errors = false;
            break;
        case 'RunUnitTestFile':
            if (array_key_exists('filename', $line))
            {
                $fileSub = SRC_DIR . $line['filename'];
                $fhSub = fopen($fileSub, 'rb');
                if ($fhSub === false)
                {
                    $output .= $prefix . ' unable to load ' . $line['filename'] . $suffix;
                    $error = true;
                    break 2;
                }
                $fileCur = $fileSub;
                $fhCur = $fhSub;
                $subLine = 0;
            }
            break;
        case 'SetErrorExpected':
            if (array_key_exists('value', $line))
            {
                if ($line['value'] == true)
                    $errorExpected = true;
                else
                    $errorExpected = false;
            }
            break;
        case 'SetUnitTestName':
            if (array_key_exists('name', $line))
                $unitTestName = $line['name'];
            $unitTestCount = 0;
            continue 2;
        case 'ShowMessage':
            if (array_key_exists('message', $line))
                $output .= $line['message'] . '<br />';
            break;
        case 'SkipBlockEnd':
            $skipProcessing = false;
            continue 2;
        case 'SkipBlockStart':
            $skipProcessing = true;
            continue 2;
        case 'StartUnitTests':
            $unitTests = true;
            continue 2;
        default:
            $output .= $prefix . ' has unknown command<br />';
            break 2;
    }

    // Call the HAL service
    if ($method !== null)
    {
        $response = callHAL($method, $resource, $args);
        $commandsProcessed++;

        // Optionally display the JSON response
        if (array_key_exists('json', $line))
            echo json_encode($response) . '<br />';

        // Check the HAL response for one or more errors
        $error = array_key_exists('error', $response);
        if ($error)
        {
            if ($errorExpected)
            {
                // Save the results
                $actualResults = $response;
            }
            else
            {
                // If there was an unexpected error then stop processing the list
                $output .= $fileCur . ' line ' . $curLine . ': service call failed: ' . $line['command'] . '<br />';
                break;
            }
        }
        else
        {
            // Perform service-specific results processing
            switch ($line['command'])
            {
                case 'CreateLogEntry':
                    // Increment the count of log entries created
                    $logEntriesCreated++;
                    break;
                case 'GetLogEntries':
                case 'GetNotificationList':
                case 'GetNotificationLogEntries':
                case 'GetNotificationRules':
                    // Save the results
                    $actualResults = $response;
                    break;
            }
        }
    }
}

displayHeader($error || $errors);

if ($output != $outputStart)
{
    $output .= '</p>';
    echo $output;
}

if ($error == false && $errors == false)
{
    if ($unitTests)
    {
        displaySuccessMessage('ALL UNIT TESTS PASSED (' . $totalUnitTestCount . ' unit tests)');
    }

    if (!$unitTests && $logEntriesCreated > 0)
    {
        echo '<br /><table class="output">';
        displayTableKeyValue('Log entries created', $logEntriesCreated);
        echo '</table>';
    }
    displayMessage($commandsProcessed . ' COMMANDS PROCESSED in ' . formatExecutionTime(time() - $timeStart));
    displayFooter();
}
else
{
    if (strlen($errorMessage) > 0)
        displayErrorMessage($errorMessage);
    elseif ($error)
        displayErrorResponse($response);

    displayFooter();
}

fclose($fhMain);
