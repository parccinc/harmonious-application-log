<?php

require_once 'guzzle_client.php';

session_start();

// Call the HAL service
$response = callHAL('PUT', '/database');

// Check the HAL response for an error
$error = array_key_exists('error', $response);

// Display the HAL response
if ($error)
{
    displayHeader($error);
    displayErrorResponse($response);
    displayFooter();
}
else
{
    // Display the database version
    header("Location: get_database_version.php");
}
