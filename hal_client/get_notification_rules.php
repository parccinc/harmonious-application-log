<?php

require_once 'guzzle_client.php';

session_start();

// Call the HAL service
$response = callHAL('GET', 'notification-rules', $_GET);

// Check the HAL response
$error = array_key_exists('error', $response);

// Display the HAL response
displayHeader($error);
if (isset($_GET['json']))
    displayJsonResponse($response);
else
{
    if ($error)
        displayErrorResponse($response);
    else
    {
        // Display the number of log entries
        $rules = sizeof($response);
        displayMessage($rules . " notification " . (($rules == 1) ? "rule" : "rules") . " returned");

        // Display the log entries
        echo '<table class="data">';
        $columns = ['notificationRuleId', 'appType', 'severity', 'eventType'];
        displayTableHeader($columns);
        foreach ($response as $notification_rule)
            displayTableDataByKey($notification_rule, $columns);
        echo '</table>';
    }
}
displayFooter();
