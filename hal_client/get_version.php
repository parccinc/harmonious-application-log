<?php

require_once 'guzzle_client.php';

session_start();

// Call the HAL service
$response = callHAL('GET', 'version');

// Check the HAL response
$error = array_key_exists('error', $response);

// Display the HAL response
displayHeader($error);
if (isset($_GET['json']))
    displayJsonResponse($response);
else
{
    if ($error)
        displayErrorResponse($response);
    else
    {
        echo '<table class="data">';
        foreach ($response as $key => $value)
            displayTableKeyValue($key, $value);
        echo '</table>';
        echo '<br />';
    }
}
displayFooter();
