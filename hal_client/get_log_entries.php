<?php

require_once 'guzzle_client.php';

session_start();

// Call the HAL service
$response = callHAL('GET', 'log-entries', $_GET);

// Check the HAL response
$error = array_key_exists('error', $response);

// Display the HAL response
displayHeader($error);
if (isset($_GET['json']))
    displayJsonResponse($response);
else
{
    if ($error)
        displayErrorResponse($response);
    else
    {
        // Display the number of log entries
        $entries = sizeof($response);
        displayMessage($entries . " log " . (($entries == 1) ? "entry" : "entries") . " returned");

        // Display the log entries
        echo '<table class="data">';
        $columns = ['appLogId', 'appDateTime', 'appType', 'severity', 'eventType', 'hostname', 'eventDesc'];
        displayTableHeader($columns);
        foreach ($response as $log_entry)
            displayTableDataByKey($log_entry, $columns);
        echo '</table>';
    }
}
displayFooter();
