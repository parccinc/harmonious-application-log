<?php

require_once 'guzzle_client.php';

session_start();

// Call the HAL service
$response = callHAL('GET', 'notification-list', $_GET);

// Check the HAL response
$error = array_key_exists('error', $response);

// Display the HAL response
displayHeader($error);
if (isset($_GET['json']))
    displayJsonResponse($response);
else
{
    if ($error)
        displayErrorResponse($response);
    else
    {
        // Display the number of log entries
        $recipients = sizeof($response);
        displayMessage($recipients . " notification " . (($recipients == 1) ? "recipient" : "recipients") . " returned");

        // Display the log entries
        echo '<table class="data">';
        $columns = ['notificationListId', 'notificationRuleId', 'appType', 'severity', 'eventType', 'recipient'];
        displayTableHeader($columns);
        foreach ($response as $recipient)
            displayTableDataByKey($recipient, $columns);
        echo '</table>';
    }
}
displayFooter();
