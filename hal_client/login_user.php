<?php

require_once 'guzzle_client.php';

session_start();

if (!isset($_SESSION['error_message']))
    $_SESSION['error_message'] = '';

// Check the client name
if (!isset($_POST['clientName']) || strlen($_POST['clientName']) == 0)
{
    $_SESSION['error_message'] = 'Username is missing';
    header("Location: index.php");
    exit;
}
$_SESSION['clientName'] = $_POST['clientName'];

// Check the password
if (!isset($_POST['password']) || strlen($_POST['password']) == 0)
{
    $_SESSION['error_message'] = 'Password is missing';
    header("Location: index.php");
    exit;
}

// Try to get an LDR session token
$response = callHAL('POST', 'session-token', ['clientName' => $_POST['clientName'], 'password' => $_POST['password']]);
if (array_key_exists('error', $response))
{
    $_SESSION['error_message'] = $response['detail'];
    header("Location: index.php");
    exit;
}

// Save the returned token
$_SESSION['hal_token'] = $response['token'];

// Display the LDR version
header("Location: get_version.php");

