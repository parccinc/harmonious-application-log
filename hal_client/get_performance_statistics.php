<?php

require_once 'guzzle_client.php';

session_start();

// Call the HAL service
$response = callHAL('GET', 'performance', $_GET);

// Check the HAL response
$error = array_key_exists('error', $response);

// Display the HAL response
displayHeader($error);
if (isset($_GET['json']))
    displayJsonResponse($response);
else
{
    if ($error)
        displayErrorResponse($response);
    else
    {
        echo '<p class="organization">' . $_GET['startDate'] . ' to ' . $_GET['endDate'] . '</p>';

        // Display the published tests
        echo '<table class="data">';
        $columns = ['serviceName', 'serviceCount', 'averageElapsedTime', 'maximumElapsedTime', 'minimumElapsedTime',
            'totalElapsedTime'];
        displayTableHeader($columns);
        foreach (array_keys($response) as $key)
        {
            // The serviceName is used as the key for each test summary in the LDR JSON response
            $service = $response[$key];
            $service['serviceName'] = $key;

            // Round the time values
            $service['averageElapsedTime'] = round($service['averageElapsedTime'], 4);
            $service['totalElapsedTime'] = round($service['totalElapsedTime'], 2);
            $service['maximumElapsedTime'] = round($service['maximumElapsedTime'], 4);
            $service['minimumElapsedTime'] = round($service['minimumElapsedTime'], 4);

            displayTableDataByKey($service, $columns);
        }
        echo '</table>';
    }
}
displayFooter();
