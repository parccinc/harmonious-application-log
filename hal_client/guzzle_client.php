<?php

require 'vendor/autoload.php';	// Import Composer-managed packages (Guzzle, etc.)

require_once '../hal_common/definitions.php';
require_once 'config.php';

// Response formats
define('RESPONSE_JSON',  '1');
define('RESPONSE_ARRAY', '2');

function callHAL($method, $resource, $args = null)
{
	// Create a Guzzle client
//    $base_url =  REQUEST_SCHEME . '://' . $_SERVER['HTTP_HOST'];
    $base_url =  REQUEST_SCHEME . '://' . HAL_HOSTNAME;
    $client = new Guzzle\Http\Client($base_url);

    // Create a Guzzle request
    switch ($method)
    {
        case 'GET':
            if ($args)
            {
                // GET arguments are placed in the query string
                $request = $client->get(HAL_RESOURCE_PATH . $resource, array(), array('query' => $args));
            }
            else
                $request = $client->get(HAL_RESOURCE_PATH . $resource);
            break;
        case 'POST':
            if ($args)
            {
                // POST arguments are placed as JSON in the request body
                $jsonData = json_encode($args);
                $request = $client->post(HAL_RESOURCE_PATH . $resource, ['content-type' => 'application/json'],
                                         $jsonData);
            }
            else
                $request = $client->post(HAL_RESOURCE_PATH . $resource);
            break;
        case 'PUT':
            if ($args)
            {
                // PUT arguments are placed as JSON in the request body
                $jsonData = json_encode($args);
                $request = $client->put(HAL_RESOURCE_PATH . $resource, ['content-type' => 'application/json'],
                    $jsonData);
            }
            else
                $request = $client->put(HAL_RESOURCE_PATH . $resource);
            break;
        case 'DELETE':
            if ($args)
            {
                // DELETE arguments are placed as JSON in the request body
                $jsonData = json_encode($args);
                $request = $client->delete(HAL_RESOURCE_PATH . $resource, ['content-type' => 'application/json'],
                                           $jsonData);
            }
            else
                $request = $client->delete(HAL_RESOURCE_PATH . $resource);
            break;
        default:
            return formatError('Request method must be GET, POST, or DELETE');
            break;
    }

    // Place the HAL session token in the HTTP header
    if (isset($_SESSION['hal_token']))
        $request->setHeader('Token', $_SESSION['hal_token']);

    // Configure the request
    $request->getCurlOptions()->set(CURLOPT_SSL_VERIFYPEER, FALSE);

	// Send the request
	try	{
		$response = $request->send();
	}
	catch (Exception $ex) {
		return formatError('Guzzle request send exception - ' . $ex->getMessage());
	}

    // Return the response
    return $response->json(); // Parse the JSON response and return an array
}

//
// HAL Client support functions
//
function compareResultArrays($expectedResults, $actualResults)
{
    foreach ($expectedResults as $key => $expectedValue)
    {
        if (array_key_exists($key, $actualResults))
        {
            if (is_array($expectedValue))
            {
                $diff = compareResultArrays($expectedValue, $actualResults[$key]);
                if ($diff !== null)
                    return $diff;
            }
            elseif ($actualResults[$key] != $expectedValue)
            {
                return '<br />returned value<br />' . '&nbsp;&nbsp;&nbsp;&nbsp;|' . $actualResults[$key] . '|<br />' .
                'does not match expected value<br />' . '&nbsp;&nbsp;&nbsp;&nbsp;|' . $expectedValue . '|<br />';
            }
        }
        else
        {
            return $key . ' value not found';
        }
    }

    return null;
}

function displayFooter()
{
?>
</body>
</html>
<?php
}

function displayHeader($error, $options = [])
{
    if (session_status() == PHP_SESSION_ACTIVE)
    {
        ?>
        <!DOCTYPE html>
        <html>
        <head>
            <link rel="stylesheet" type="text/css" href="hal_client.css" />
        </head>
        <body <?php
            if (array_key_exists('focus', $options))
                echo 'onload="document.main.' . $options['focus'] . '.focus();"';
        ?>>
        <?php
            if ($error)
                showStatusFail();
            else
                showStatusOkay();

            if (array_key_exists('databaseVersionError', $options))
            {
                if ($options['databaseVersionError'])
                    showDatabaseVersionFail();
                else
                    showDatabaseVersionOkay();
            }

            if (isset($_SESSION['hal_token']))
            {
                ?>
                <table class="header">
                    <tr>
                        <th>HAL Client</th>
                        <th>
                            <a href="logout_user.php">LOG OUT</a>
                        </th>
                    </tr>
                </table>
                <?php
            }
    }
    else
    {
        ?>
        <!DOCTYPE html>
        <html>
        <head>
            <link rel="stylesheet" type="text/css" href="hal_client.css" />
        </head>
        <body>
        <?php
            if ($error)
                showStatusFail();
            else
                showStatusOkay();
    }
}

function displayErrorMessage($message)
{
    echo '<p class="error">';
    echo $message;
    echo '</p>';
}

function displayErrorResponse($response)
{
    echo '<p class="error">';
    foreach ($response as $key => $value)
        echo $key . ' => ' . $value . '<br />';
    echo '</p>';
}

function displaySuccessMessage($message)
{
    echo '<p class="success">';
    echo $message;
    echo '</p>';
}

function displayWarningMessage($message)
{
    echo '<p class="warning">';
    echo $message;
    echo '</p>';
}

function formatExecutionTime($seconds)
{
    $hours = intval($seconds / SECONDS_PER_HOUR);
    $seconds = $seconds % SECONDS_PER_HOUR;
    $minutes = intval($seconds / 60);
    $seconds = $seconds % 60;

    $executionTime = '';
    if ($hours > 1)
        $executionTime .= $hours . ' hours ';
    else if ($hours == 1)
        $executionTime .= $hours . ' hour ';
    if ($minutes > 1)
        $executionTime .= $minutes . ' minutes ';
    else if ($minutes == 1)
        $executionTime .= $minutes . ' minute ';
    if ($seconds > 1)
        $executionTime .= $seconds . ' seconds';
    else if ($seconds == 1)
        $executionTime .= $seconds . ' second';

    if (strlen($executionTime) == 0)
        $executionTime = "less than 1 second";

    return $executionTime;
}

function displayJsonResponse($response)
{
    echo '<p class="json">';
    echo json_encode($response);
    echo '</p>';
}

function displayMessage($message)
{
    echo '<p class="message">';
    echo $message;
    echo '</p>';
}

function displayTableDataByIndex($data)
{
	echo '<tr>';
	foreach ($data as $datum)
		echo '<td><nobr>' . $datum . '</nobr></td>';
	echo '</tr>';
}

function displayTableDataByKey($data, $keys)
{
	echo '<tr>';
	foreach ($keys as $key)
		echo '<td><nobr>' . $data[$key] . '</nobr></td>';
	echo '</tr>';
}

function displayTableHeader($headings)
{
	echo '<tr>';
	foreach ($headings as $heading)
		echo '<th>' . $heading . '</th>';
	echo '</tr>';
}

function displayTableKeyValue($key, $value)
{
    echo '<tr>';
    echo '<td><nobr>' . $key . '</nobr></td><td><nobr>' . $value . '</nobr></td>';
    echo '</tr>';
}

function formatError($message)
{
    return ['error' => $message];
}

function showDatabaseVersionFail()
{
    echo '<p class="status">DATABASE: FAIL</p>';
}

function showDatabaseVersionOkay()
{
    echo '<p class="status">DATABASE: OKAY</p>';
}

function showStatusFail()
{
    echo '<p class="status">STATUS: FAIL</p>';
}

function showStatusOkay()
{
    echo '<p class="status">STATUS: OKAY</p>';
}
