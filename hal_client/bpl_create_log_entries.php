<?php

require_once 'guzzle_client.php';

define('SRC_DIR', 'process/');

set_time_limit(0);

session_start();

// Get required parameters
if (!isset($_GET['startDate']))
{
    displayHeader(true);
    displayErrorMessage('Please specify the start date - e.g. startDate=2015-04-01');
    displayFooter();
    exit;
}
$logTimeStamp = strtotime($_GET['startDate']);
if ($logTimeStamp === false)
{
    displayHeader(true);
    displayErrorMessage('startDate ' . $_GET['startDate'] . ' is not understood');
    displayFooter();
    exit;
}
if (!isset($_GET['count']))
{
    displayHeader(true);
    displayErrorMessage('Please specify the number of log entries - e.g. count=2000');
    displayFooter();
    exit;
}

// Open the output file
$filename = SRC_DIR . 'create_' . $_GET['count'] . '_log_entries_starting_on_' . $_GET['startDate'] . '.json';
$fout = fopen($filename, 'wb');
if ($fout === false)
{
    $error = true;
    displayHeader($error);
    displayErrorMessage('Unable to open file ' . $filename);
    displayFooter();
    exit;
}

$baseHostname = '.stable.parcc-ads.breaktech.org';

// Define the pool of application events
$appEvents =
[
    ['ACR', '5', '5000', 'Instance startup'],
    ['ADP', '5', '5000', 'Instance startup'],
    ['LDR', '5', '5000', 'Instance startup'],
    ['PRC', '5', '5000', 'Instance startup'],
    ['TAP', '5', '5000', 'Instance startup'],
    ['ACR', '5', '5001', 'Instance shutdown'],
    ['ADP', '5', '5001', 'Instance shutdown'],
    ['LDR', '5', '5001', 'Instance shutdown'],
    ['PRC', '5', '5001', 'Instance shutdown'],
    ['TAP', '5', '5001', 'Instance shutdown'],
    ['ACR', '1', '1000', 'Database connection lost'],
    ['ADP', '1', '1000', 'Database connection lost'],
    ['DMR', '1', '1000', 'Database connection lost'],
    ['LDR', '1', '1000', 'Database connection lost'],
    ['PRC', '1', '1000', 'Database connection lost'],
    ['TAP', '1', '1000', 'Database connection lost'],
    ['ACR', '1', '1001', 'Database connection restored'],
    ['ADP', '1', '1001', 'Database connection restored'],
    ['DMR', '1', '1001', 'Database connection restored'],
    ['LDR', '1', '1001', 'Database connection restored'],
    ['PRC', '1', '1001', 'Database connection restored'],
    ['TAP', '1', '1001', 'Database connection restored'],
    ['ACR', '6', '6000', 'CPU utilization above 70% - creating new instance'],
    ['ADP', '6', '6000', 'CPU utilization above 70% - creating new instance'],
    ['LDR', '6', '6000', 'CPU utilization above 70% - creating new instance'],
    ['PRC', '6', '6000', 'CPU utilization above 70% - creating new instance'],
    ['TAP', '6', '6000', 'CPU utilization above 70% - creating new instance'],
    ['ACR', '6', '6001', 'CPU utilization below 5% - removing self instance'],
    ['ADP', '6', '6001', 'CPU utilization below 5% - removing self instance'],
    ['LDR', '6', '6001', 'CPU utilization below 5% - removing self instance'],
    ['PRC', '6', '6001', 'CPU utilization below 5% - removing self instance'],
    ['TAP', '6', '6001', 'CPU utilization below 5% - removing self instance'],
    ['ACR', '4', '4000', 'File system storage is above 80%'],
    ['ADP', '4', '4000', 'File system storage is above 80%'],
    ['LDR', '4', '4000', 'File system storage is above 80%'],
    ['PRC', '4', '4000', 'File system storage is above 80%'],
    ['TAP', '4', '4000', 'File system storage is above 80%'],
    ['ACR', '4', '4001', 'File system storage is back below 80%'],
    ['ADP', '4', '4001', 'File system storage is back below 80%'],
    ['LDR', '4', '4001', 'File system storage is back below 80%'],
    ['PRC', '4', '4001', 'File system storage is back below 80%'],
    ['TAP', '4', '4001', 'File system storage is back below 80%'],
    ['ACR', '3', '3000', 'Database response time is slow'],
    ['ADP', '3', '3000', 'Database response time is slow'],
    ['DMR', '3', '3000', 'Database response time is slow'],
    ['LDR', '3', '3000', 'Database response time is slow'],
    ['PRC', '3', '3000', 'Database response time is slow'],
    ['TAP', '3', '3000', 'Database response time is slow'],
    ['ACR', '3', '3001', 'Database response time is back to normal'],
    ['ADP', '3', '3001', 'Database response time is back to normal'],
    ['DMR', '3', '3001', 'Database response time is back to normal'],
    ['LDR', '3', '3001', 'Database response time is back to normal'],
    ['PRC', '3', '3001', 'Database response time is back to normal'],
    ['TAP', '3', '3001', 'Database response time is back to normal'],
    ['ACR', '1', '1002', 'Cache server connection lost'],
    ['ADP', '1', '1002', 'Cache server connection lost'],
    ['LDR', '1', '1002', 'Cache server connection lost'],
    ['PRC', '1', '1002', 'Cache server connection lost'],
    ['TAP', '1', '1002', 'Cache server connection lost'],
    ['ACR', '1', '1003', 'Cache server connection restored'],
    ['ADP', '1', '1003', 'Cache server connection restored'],
    ['LDR', '1', '1003', 'Cache server connection restored'],
    ['PRC', '1', '1003', 'Cache server connection restored'],
    ['TAP', '1', '1003', 'Cache server connection restored'],
    ['ADP', '2', '2200', 'LDR service calls are failing'],
    ['LDR', '2', '2300', 'ADP service calls are failing'],
    ['TAP', '2', '2500', 'LDR service calls are failing'],
    ['ADP', '2', '2201', 'LDR service calls are working again'],
    ['LDR', '2', '2301', 'ADP service calls are working again'],
    ['TAP', '2', '2501', 'LDR service calls are working again'],
    ['DMR', '2', '2600', 'OpenAM server calls are failing'],
    ['TAP', '2', '2502', 'OpenAM server calls are failing'],
    ['DMR', '2', '2601', 'OpenAM server calls are working again'],
    ['TAP', '2', '2503', 'OpenAM server calls are working again'],
    ['DMR', '6', '6600', 'Ingestion was successful'],
    ['DMR', '3', '3601', 'Ingestion file decyption failed'],
    ['DMR', '3', '3601', 'Ingestion file signature verification failed'],
    ['DMR', '3', '3602', 'Ingestion files not found'],
    ['DMR', '3', '3603', 'Ingestion CSV file validation failed'],
    ['DMR', '3', '3603', 'Ingestion JSON file validation failed'],
    ['DMR', '3', '3603', 'Ingestion XML file validation failed'],
    ['DMR', '3', '3604', 'Ingestion JSON valid load type not found'],
    ['DMR', '3', '3604', 'Ingestion JSON valid subject type not found'],
    ['DMR', '3', '3605', 'Ingestion CSV data validation failed'],
    ['DMR', '3', '3605', 'Ingestion JSON data validation failed'],
    ['DMR', '3', '3605', 'Ingestion XML data validation failed'],
    ['DMR', '3', '3606', 'Ingestion database loading failed'],
    ['DMR', '6', '6620', 'Analytics refresh was successful'],
    ['DMR', '6', '6621', 'Analytics refresh failed'],
    ['DMR', '6', '6623', 'Analytics refresh timed out'],
    ['DMR', '3', '6665', 'Missed heartbeats reached threshold'],
    ['ACR', '4', '4002', 'Memory utilization is above 90%'],
    ['ADP', '4', '4002', 'Memory utilization is above 90%'],
    ['LDR', '4', '4002', 'Memory utilization is above 90%'],
    ['PRC', '4', '4002', 'Memory utilization is above 90%'],
    ['TAP', '4', '4002', 'Memory utilization is above 90%'],
    ['ACR', '4', '4003', 'Memory utilization is back below 90%'],
    ['ADP', '4', '4003', 'Memory utilization is back below 90%'],
    ['LDR', '4', '4003', 'Memory utilization is back below 90%'],
    ['PRC', '4', '4003', 'Memory utilization is back below 90%'],
    ['TAP', '4', '4003', 'Memory utilization is back below 90%']
];
$maxAppEventIdx = sizeof($appEvents) - 1;

// Write the output file header
$out = '# Create ' . $_GET['count'] . ' log entries starting on ' . $_GET['startDate'] . "\n";
if (fwrite($fout, $out) === false)
{
    displayHeader(true);
    displayErrorMessage('Unable to write header to file ' . $filename);
    displayFooter();
    fclose($fout);
    exit;
}

// Write the CreateLogEntry commands
for ($count = 0; $count < $_GET['count']; $count++)
{
    // Select an application event
    $idx = mt_rand(0, $maxAppEventIdx);

    $appType = $appEvents[$idx][0];

    $hostname = strtolower($appType) . mt_rand(1, 6) . $baseHostname;

    $logTimeStamp += mt_rand(0,900);
    $appDateTime = date("Y-m-d H:i:s", $logTimeStamp);

    $severity = $appEvents[$idx][1];

    $eventType = $appEvents[$idx][2];

    $eventDesc = $appEvents[$idx][3];

    $command =
        [
            'command' => 'CreateLogEntry',
            'appDateTime' => $appDateTime,
            'appType' => $appType,
            'hostname' => $hostname,
            'severity' => $severity,
            'eventType' => $eventType,
            'eventDesc' => $eventDesc
        ];
    $jsonCommand = json_encode($command) . "\n";

    if (fwrite($fout, $jsonCommand) === false)
    {
        displayHeader(true);
        displayErrorMessage('Unable to write command to file ' . $filename);
        displayFooter();
        fclose($fout);
        exit;
    }
}

displayHeader(false);
displayMessage('File ' . $filename . ' written successfully');
displayFooter();
fclose($fout);
