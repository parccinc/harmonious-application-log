<?php

require_once 'guzzle_client.php';

session_start();

// Delete the LDR token
if (isset($_SESSION['hal_token']))
    callHAL('DELETE', 'session-token', RESPONSE_ARRAY);

// Destroy the session
$_SESSION = array();
if (isset($_COOKIE[session_name()]))
    setcookie(session_name(), '', time() - 3600);
session_destroy();

header("Location: index.php");
