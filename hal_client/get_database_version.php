<?php

require_once 'guzzle_client.php';

session_start();

// Call the HAL service
$response = callHAL('GET', '/database/version');

// Check the HAL response for an error
$error = array_key_exists('error', $response);

// Display the HAL response
if ($error)
{
    displayHeader($error);
    displayErrorResponse($response);
    displayFooter();
}
elseif (isset($_GET['json']))
{
    displayHeader($error);
    displayJsonResponse($response);
    displayFooter();
}
else
{
    // Check the database versions against the application software required version
    $databaseClass = ($response['databaseVersion'] == $response['requiredVersion']) ? 'good' : 'bad';

    // Set the database status for CI
    if ($databaseClass == 'bad')
        $databaseVersionError = true;
    else
        $databaseVersionError = false;
    displayHeader($error, ['databaseVersionError' => $databaseVersionError]);

    // Display the database versions
    echo '<table class="data">';
    echo '<td>Required Database Version</td><td class="good">' . $response['requiredVersion'] . '</td>';
    echo '<td>Database Version</td><td class="' . $databaseClass . '">' . $response['databaseVersion'] . '</td>';
    echo '</table>';
    echo '<br />';
    displayFooter();
}
