<?php

require_once 'guzzle_client.php';

session_start();

// Call the HAL service
$response = callHAL('GET', 'tables');

// Check the HAL response
$error = array_key_exists('error', $response);

// Display the HAL response
displayHeader($error);
if (isset($_GET['json']))
    displayJsonResponse($response);
else
{
    if ($error)
        displayErrorResponse($response);
    else
    {
        echo '<p class="message">';
        if (sizeof($response) > 0)
        {
            foreach ($response as $table)
                echo $table . '<br />';
        }
        else
            echo "There are no tables";
        echo '</p>';
    }
}
displayFooter();
