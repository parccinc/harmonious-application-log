<?php

require_once 'guzzle_client.php';

session_start();

if (!isset($_GET['notificationRuleId']))
{
    displayHeader(true);
    displayErrorMessage('notificationRuleId value is required in the query string');
    displayFooter();
    exit;
}

// Prepare the resource path
$resource = 'notification-rule/' . $_GET['notificationRuleId'];

// Call the HAL service
$response = callHAL('DELETE', $resource, $_GET);

// Check the HAL response
$error = array_key_exists('error', $response);

// Display the HAL response
displayHeader($error);
if (isset($_GET['json']))
    displayJsonResponse($response);
else
{
    if ($error)
        displayErrorResponse($response);
    else
    {
        echo '<p class="message">';
        foreach ($response as $key => $value)
            echo $key . ' => ' . $value . '<br />';
        echo '</p>';
    }
}
displayFooter();
