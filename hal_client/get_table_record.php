<?php

require_once 'guzzle_client.php';

session_start();

// Call the HAL service
$response = callHAL('GET', '?????', $_GET);

// Check the HAL response
$error = array_key_exists('error', $response);

// Display the HAL response
displayHeader($error);
if (isset($_GET['json']))
    displayJsonResponse($response);
else
{
    // Check if an error occurred
    if (array_key_exists('error', $response))
        displayErrorResponse($response);
    else
    {
        // Display the record
        echo '<p class="organization">' . $_GET['tableName'] . '</p>';

        echo '<table class="data">';
        $columns = ['Column', 'Value'];
        displayTableHeader($columns);
        foreach ($response as $key => $value)
        {
            displayTableKeyValue($key, $value);
        }
        echo '</table>';
    }
}
displayFooter();
