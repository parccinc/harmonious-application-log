#!/bin/sh
# This script creates database and user
# Usage ./dbcreate.sh dbhost=dbhost dbname=name user=user dbpwd=password mysqluser=user mysqlpwd=password host=host
# dbhost is database host. Default is localhost.
# dbname is database name
# user is user name part of database account to be created and granted all permissions to database
# dbpwd is database user password
# mysqluser is user on MySQL
# mysqlpwd is password for user on MySQL
# host is host name part of database account

LOGFILE=dbcreate.sh.log
echo `date` >$LOGFILE
DBHOST=
DBNAME=
MYSQLUSER=
MYSQLPWD=
USER=
PWD=
HOST=

log() {
	COLOR=
	if [ $# -gt 1 ];then
		case "$2" in
			error)
				COLOR="31"
				;;

			warn)
				COLOR="33"
				;;
					
			ok)
				COLOR="32"
				;;
		esac
	fi

	local MSG=
	if [ -z "$COLOR" ];then
		MSG="$1"
	else
		MSG="\\E[0;""$COLOR""m""$1""\\E[0m"
	fi

	if [ -z "$LOGFILE" ];then
		echo -e "$MSG"
	else
		echo -e "$MSG" | tee -a $LOGFILE
	fi
}

if [ $# -gt 0 ];then
	for (( i=1; i<=$#; i++ ));do
		IFS='=' read -ra ARR <<< "${@:i:1}"
		case "${ARR[0]}" in
			dbhost)
				DBHOST="${ARR[1]}"
				;;
				
			dbname)
				DBNAME="${ARR[1]}"
				;;

			user)
				USER="${ARR[1]}"
				;;
					
			dbpwd)
				PWD="${ARR[1]}"
				;;

			mysqluser)
				MYSQLUSER="${ARR[1]}"
				;;
				
			mysqlpwd)
				MYSQLPWD="${ARR[1]}"
				;;
				
			host)
				HOST="${ARR[1]}"
				;;
		esac
	done
fi

if [ -z "$DBNAME" ];then
	log "Database name is required." "error"
	exit 1
fi

if [ -z "$USER" ];then
	log "Database user name is required." "error"
	exit 1
fi

if [ -z "$PWD" ];then
	log "Database user password is required." "error"
	exit 1
fi

if [ -z "$MYSQLUSER" ];then
	log "MySQL user is required." "error"
	exit 1
fi

if [ -z "$MYSQLPWD" ];then
	log "MySQL password is required." "error"
	exit 1
fi

if [ -z "$HOST" ];then
	log "HAL host is required." error
	exit 1
fi

execcmd() {
	log "$1"
	local ERR=$((eval $1 >>$LOGFILE) 2>&1)
	if [ $? -gt 0 ];then
		log "$ERR" "error"
		exit 1
	fi
}

dbexists() {
	local RESULT=$($MYSQL "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = '$DBNAME'")
	if [ -z "$RESULT" ];then
		echo false
	else
		echo true
	fi
}

userexists() {
	local RESULT=$($MYSQL "SELECT User FROM mysql.user WHERE User='$USER' AND Host='$HOST'")
	if [ -z "$RESULT" ];then
		echo false
	else
		echo true
	fi
}

if [ -z "$DBHOST" ];then
	MYSQL="mysql -u $MYSQLUSER -p$MYSQLPWD -s -N -e"
else
	MYSQL="mysql -h $DBHOST -u $MYSQLUSER -p$MYSQLPWD -s -N -e"
fi

RESULT=$(dbexists)
if [ "$RESULT" = true ];then
	log "Database $DBNAME exists. Nothing to create!" warn
else
	execcmd "$MYSQL 'CREATE DATABASE $DBNAME'"
	RESULT=$(dbexists)
	if [ "$RESULT" = true ];then
		log "Database $DBNAME created!" ok
	else
		log "Failed to create database $DBNAME." error
		exit 1
	fi
fi

RESULT=$(userexists)
if [ "$RESULT" = true ];then
	log "Database user $USER exists. Nothing to create!" warn
else
	execcmd "$MYSQL 'CREATE USER '\\''$USER'\\''@'\\''$HOST'\\'"
	execcmd "$MYSQL 'SET PASSWORD FOR '\\''$USER'\''@'\\''$HOST'\\'' = PASSWORD('\\''$PWD'\\'')'"
	RESULT=$(userexists)
	if [ "$RESULT" = true ];then
		log "Database user $USER@$HOST created!" ok
	else
		log "Failed to create user $USER@$HOST." error
		exit 1
	fi
fi

execcmd "$MYSQL 'GRANT ALL ON $DBNAME.* TO '\\''$USER'\\''@'\\''$HOST'\\'"
log "Granted permissions for $USER@$HOST on $DBNAME!" ok

exit 0