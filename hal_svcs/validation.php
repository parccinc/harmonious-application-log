<?php

//
// LDR data validation functions
//
// These functions expect a database connection as the second argument
//
// validateArg() ............................ 2015-03-13 refactored
//
// validateToken() .......................... 2014-11-17 refactored
//

$appTypes = ['ACR', 'ADP', 'DMR', 'LDR', 'PRC', 'TAP'];

$argFilterMap =
[
    'appDateTime' => DFT_DATETIME,
    'appLogId' => DFT_NATURAL_NUMBER,
    'appType' => DFT_ALPHA_MIXED,
    'clientName' => DFT_ALPHANUM_MIXED_PUNCT_A,
    'endDate' => DFT_DATE,
    'endDateTime' => DFT_DATETIME,
    'eventDesc' => DFT_NONE,
    'eventType' => DFT_NATURAL_NUMBER,
    'eventType1' => DFT_NATURAL_NUMBER,
    'eventType2' => DFT_NATURAL_NUMBER,
    'hostname' => DFT_ALPHANUM_MIXED_PUNCT_D,
    'notificationListId' => DFT_NATURAL_NUMBER,
    'notificationRuleId' => DFT_NATURAL_NUMBER,
    'pageLimit' => DFT_NATURAL_NUMBER,
    'pageOffset' => DFT_NATURAL_NUMBER,
    'password' => DFT_ALPHANUM_MIXED,
    'recipient' => DFT_EMAIL_ADDRESS,
    'severity' => DFT_NATURAL_NUMBER,
    'startDate' => DFT_DATE,
    'startDateTime' => DFT_DATETIME,
    'tableName' => DFT_ALPHA_MIXED_PUNCT_A
];

// validateArg()
//   Returns a validated argument from an array of key-value pairs
//     OR
//   Returns an ErrorHAL object if there is an error
//
function validateArg(PDO $db, $args, $argName, $dft, $dco)
{
    global $valueColumnNames, $columnTableNames;

    // Get the argument
    $arg = null;
    if (array_key_exists($argName, $args))
        $arg = $args[$argName];

    // Check if the argument is required
    if ($dco & DCO_REQUIRED)
    {
        if (strlen($arg) == 0)
        {
            $errorHAL = new ErrorHAL(HAL_EC_ARG_NOT_PROVIDED, 'Required value ' . $argName . ' not provided');
            $errorHAL->logError(__FUNCTION__);
            return $errorHAL;
        }
    }

    // Check if the argument should be converted to lower case
    if ($dco & DCO_TOLOWER)
        $arg = strtolower($arg);

    // Check if the argument should be converted to upper case
    if ($dco & DCO_TOUPPER)
        $arg = strtoupper($arg);

    // If the argument is non-empty
    if (strlen($arg) > 0)
    {
        // Check if the argument contains any invalid characters
        if ($dft != DFT_NONE && hasInvalidCharacters($arg, $dft))
        {
            $errorHAL = new ErrorHAL(HAL_EC_ARG_INVALID_CHARACTERS, 'This ' . $argName .
                                     ' has invalid characters: ' . $arg);
            $errorHAL->logError(__FUNCTION__);
            return $errorHAL;
        }

        // Check if the argument must exist in the database
        if ($dco & DCO_MUST_EXIST)
        {
            // Get the column name
            if (array_key_exists($argName, $valueColumnNames))
                $columnName = $valueColumnNames[$argName];
            else
            {
                $errorHAL = new ErrorHAL(HAL_EC_UNSPECIFIED_ERROR_CODE,
                                         'No column name is defined for this parameter: ' . $argName);
                $errorHAL->logError(__FUNCTION__);
                return $errorHAL;
            }

            // Get the table name
            if (array_key_exists($columnName, $columnTableNames))
                $tableName = $columnTableNames[$columnName];
            else
            {
                $errorHAL = new ErrorHAL(HAL_EC_UNSPECIFIED_ERROR_CODE,
                                         'No table name is defined for this parameter: ' . $argName);
                $errorHAL->logError(__FUNCTION__);
                return $errorHAL;
            }

            // Check that the argument exists
            $valueExists = dbTableValueExists($db, $tableName, $columnName, $arg);
            if ($valueExists instanceof ErrorHAL)
            {
                $valueExists->logError(__FUNCTION__);
                return $valueExists;
            }
            else if ($valueExists === false)
            {
                $errorHAL = new ErrorHAL(HAL_EC_DATABASE_RECORD_NOT_FOUND,
                                         HAL_ED_DATABASE_RECORD_NOT_FOUND . $argName . ' ' . $arg);
                $errorHAL->logError(__FUNCTION__);
                return $errorHAL;
            }
        }
    }

    return $arg;
}

// validateDateTime()
//   Returns a validated dateTime value
//     OR
//   Returns an ErrorLDR object if there is an error
//
function validateDateTime($dateTime, $argName)
{
    // Get the date and time values. The time value is optional.
    $checkTime = true;
    $args = explode(' ', $dateTime);
    if (sizeof($args) == 1)
    {
        $date = $args[0];
        $checkTime = false;
        $time = null;
    }
    elseif (sizeof($args) == 2)
    {
        $date = $args[0];
        $time = $args[1];
    }
    else
    {
        $errorHAL = new ErrorHAL(HAL_EC_DATETIME_INVALID_FORMAT, 'The date/time format of this ' .  $argName .
                                 ' is not YYYY-MM-DD HH:MM:SS or YYYY-MM-DD: ' . $dateTime);
        $errorHAL->logError(__FUNCTION__);
        return $errorHAL;
    }

    // Check the date format
    if (!preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/", $date))
    {
        $errorHAL = new ErrorHAL(HAL_EC_DATE_INVALID_FORMAT, 'The date format of this ' .  $argName .
                                 ' is not YYYY-MM-DD: ' . $dateTime);
        $errorHAL->logError(__FUNCTION__);
        return $errorHAL;
    }

    // Check the time format
    if ($checkTime && !preg_match("/^[0-9]{2}:[0-9]{2}:[0-9]{2}$/", $time))
    {
        $errorHAL = new ErrorHAL(HAL_EC_TIME_INVALID_FORMAT, 'The time format of this ' .  $argName .
                                 ' is not HH:MM:SS: ' . $dateTime);
        $errorHAL->logError(__FUNCTION__);
        return $errorHAL;
    }

    // Check the date
    list($year, $month, $day) = explode('-', $date);
    if (!checkdate(intval($month), intval($day), intval($year)))
    {
        $errorHAL = new ErrorHAL(HAL_EC_DATE_INVALID_DATE,
                                 'This ' .  $argName . ' date is not a valid date: ' . $dateTime);
        $errorHAL->logError(__FUNCTION__);
        return $errorHAL;
    }

    // Check the time
    if ($checkTime)
    {
        list($hour, $minute, $second) = explode(':', $time);
        if (!checktime(intval($hour), intval($minute), intval($second)))
        {
            $errorHAL = new ErrorHAL(HAL_EC_TIME_INVALID_TIME,
                                     'This ' .  $argName . ' time is not a valid time: ' . $dateTime);
            $errorHAL->logError(__FUNCTION__);
            return $errorHAL;
        }
    }

    return $dateTime;
}

// validateToken()
//   Returns a validated token
//     OR
//   Returns an ErrorLDR object if the token validation fails
//
function validateToken($db, $token)
{
    // Check if a token was passed
    if (strlen($token) == 0)
    {
        $errorHAL = new ErrorHAL(HAL_EC_TOKEN_NOT_PROVIDED, 'Session token not provided');
        $errorHAL->logError(__FUNCTION__);
        return $errorHAL;
    }

    // Check the token length
    if (strlen($token) != LENGTH_SESSION_TOKEN)
    {
        $errorHAL = new ErrorHAL(HAL_EC_TOKEN_LENGTH_INVALID, HAL_ED_TOKEN_LENGTH_INVALID);
        $errorHAL->logError(__FUNCTION__);
        return $errorHAL;
    }

    // Filter the token
    if (hasInvalidCharacters($token, DFT_HEXADECIMAL_UPPER))
    {
        $errorHAL = new ErrorHAL(HAL_EC_TOKEN_INVALID_CHARACTERS, HAL_ED_TOKEN_INVALID_CHARACTERS . $token);
        $errorHAL->logError(__FUNCTION__);
        return $errorHAL;
    }

    // Check if the token exists
    $dateTime = dbGetSessionDatetime_By_Token($db, $token);
    if ($dateTime instanceof ErrorHAL)
    {
        $dateTime->logError(__FUNCTION__);
        return $dateTime;
    }
    if ($dateTime == null)
    {
        $errorHAL = new ErrorHAL(HAL_EC_TOKEN_NOT_FOUND, HAL_ED_TOKEN_NOT_FOUND);
        $errorHAL->logError(__FUNCTION__);
        return $errorHAL;
    }

    // Check if the token has expired
    if (time() - strtotime($dateTime) > TOKEN_TIMEOUT)
    {
        $errorHAL = new ErrorHAL(HAL_EC_TOKEN_HAS_EXPIRED, HAL_ED_TOKEN_HAS_EXPIRED);
        $errorHAL->logError(__FUNCTION__);
        return $errorHAL;
    }

    // Token is valid so make it current
    $touch = dbTouchSessionToken($db, $token);
    if ($touch instanceof ErrorHAL)
    {
        $touch->logError(__FUNCTION__);
        return $touch;
    }

    // Return the validated token
    return $token;
}

//
// Helper functions
//
function checktime($hour, $minute, $second)
{
    if ($hour < 0 || $hour > 23)
        return false;

    if ($minute < 0 || $minute > 59)
        return false;

    if ($second < 0 || $second > 59)
        return false;

    return true;
}

function createValueError($name, $arg)
{
    $errorHAL = new ErrorHAL(HAL_EC_ARG_INVALID_VALUE, 'This ' . $name . ' value is invalid: ' . $arg);
    return $errorHAL;
}

function hasInvalidCharacters($arg, $dft)
{
    $regex = "/[^" . $dft . "]/";

    $result = preg_match($regex, $arg);

    return $result;
}
