<?php

define('HAL_DATABASE_VERSION', '0.2.3');

//
// HAL database functions
//
// Most of these functions expect a PDO object as the first argument
//
// dbAddPerformanceRecord() [N/A]
// dbConnect() [N/A]
// dbCreateLogEntry()
// dbCreateNotificationLogEntry()
// dbCreateNotificationRecipient()
// dbCreateNotificationRule()
// dbCreateSessionToken() [N/A]
// dbCreateTable() [N/A]
// dbDeleteNotificationRecipient()
// dbDeleteNotificationRule()
// dbDeleteSessionToken() [IDX]
// dbDropAllTables() [N/A]
// dbDropTable() [N/A]
// dbGetDatabaseVersion()
// dbGetLogEntries() [IDX]
// dbGetMatchingNotificationRules()
// dbGetNotificationList()
// dbGetNotificationListId()
// dbGetNotificationLogEntries()
// dbGetNotificationRuleId()
// dbGetNotificationRuleRecipients()
// dbGetNotificationRules()
// dbGetPerformanceRecord() [IDX]
// dbGetPerformanceStatistics() [IDX]
// dbGetSessionDatetime_By_Token() [IDX]
// dbGetTableNames() [N/A]
// dbMigrateDatabase()
// dbMigrateDatabaseBackward()
// dbMigrateDatabaseForward()
// dbRecordPerformance()
// dbTableValueExists() [N/A]
// dbTouchSessionToken() [IDX]
// dbUpdatePerformanceRecord() [N/A]
// errorQueryException()

// HAL database tables
$dbTables =
[
    'app_log' =>
    [
        'create' =>
            '(appLogId INT NOT NULL AUTO_INCREMENT,
              logDateTime DATETIME NOT NULL,
              appDateTime DATETIME NOT NULL,
              dateTimeDelta INT NOT NULL,
              appType CHAR(' . LENGTH_APP_TYPE . ') NOT NULL,
              severity CHAR(' . LENGTH_SEVERITY . ') NOT NULL,
              eventType CHAR(' . LENGTH_EVENT_TYPE . ') NOT NULL,
              hostname VARCHAR(' . MAXLEN_HOSTNAME . ') NOT NULL,
              eventDesc VARCHAR(' . MAXLEN_EVENT_DESC . ') NOT NULL,
              PRIMARY KEY (appLogId),
              INDEX (logDateTime),
              INDEX (appDateTime),
              INDEX (appType),
              INDEX (severity),
              INDEX (eventType))',

        'primaryKey' => 'appLogId'
    ],

    'client' =>
    [
        'create' =>
            '(clientId INT NOT NULL AUTO_INCREMENT,
              clientName VARCHAR(40) NOT NULL,
              clientPassword CHAR(128),
              PRIMARY KEY (clientId),
              UNIQUE (clientName))',

        'primaryKey' => 'clientId'
    ],

    'notification_list' =>
    [
        'create' =>
            '(notificationListId INT NOT NULL AUTO_INCREMENT,
              notificationRuleId INT NOT NULL,
              recipient VARCHAR(' . MAXLEN_RECIPIENT . ') NOT NULL,
              deleted ENUM("Y", "N") NOT NULL DEFAULT "N",
              PRIMARY KEY (notificationListId),
              INDEX (notificationRuleId),
              INDEX (recipient))',

        'primaryKey' => 'notificationListId'
    ],

    'notification_log' =>
    [
        'create' =>
            '(notificationLogId INT NOT NULL AUTO_INCREMENT,
              logDateTime DATETIME NOT NULL,
              notificationRuleId INT NOT NULL,
              appLogId INT NOT NULL,
              recipient VARCHAR(' . MAXLEN_RECIPIENT . ') NOT NULL,
              PRIMARY KEY (notificationLogId),
              INDEX (logDateTime),
              INDEX (notificationRuleId),
              INDEX (recipient))',

        'primaryKey' => 'notificationLogId'
    ],

    'notification_rule' =>
    [
        'create' =>
            '(notificationRuleId INT NOT NULL AUTO_INCREMENT,
              appType CHAR(' . LENGTH_APP_TYPE . ') NOT NULL,
              severity CHAR(' . LENGTH_SEVERITY . ') NOT NULL,
              eventType CHAR(' . LENGTH_EVENT_TYPE . ') NOT NULL,
              deleted ENUM("Y", "N") NOT NULL DEFAULT "N",
              PRIMARY KEY (notificationRuleId),
              INDEX (appType),
              INDEX (severity),
              INDEX (eventType))',

        'primaryKey' => 'notificationRuleId'
    ],

    'performance' =>
    [
        'create' =>
            '(performanceId INT NOT NULL AUTO_INCREMENT,
              statisticsDate DATE NULL,
              serviceName VARCHAR(60) NULL,
              serviceCount INT NULL,
              totalElapsedTime DOUBLE NULL,
              maximumElapsedTime DOUBLE NULL,
              minimumElapsedTime DOUBLE NULL,
              PRIMARY KEY (performanceId),
              INDEX (statisticsDate, serviceName))',

        'primaryKey' => 'performanceId'
    ],

    'session_token' =>
    [
        'create' =>
            '(sessionTokenId INT NOT NULL AUTO_INCREMENT,
              clientId INT NOT NULL,
              token CHAR(' . LENGTH_SESSION_TOKEN . ') NOT NULL,
              dateTime DATETIME NOT NULL,
              PRIMARY KEY (sessionTokenId),
              UNIQUE (token))',

        'primaryKey' => 'sessionTokenId'
    ],

    'version_history' =>
    [
        'create' =>
            '(versionHistoryId INT NOT NULL AUTO_INCREMENT,
              version VARCHAR(8) NOT NULL,
              dateTime DATETIME NOT NULL,
              PRIMARY KEY (versionHistoryId),
              INDEX (version),
              INDEX (dateTime))',

        'primaryKey' => 'versionHistoryId'
    ]
];

$valueColumnNames =
[
    'notificationListId' => 'notificationListId',
    'notificationRuleId' => 'notificationRuleId'
];

$columnTableNames =
[
    'notificationListId' => 'notification_list',
    'notificationRuleId' => 'notification_rule'
];

// HAL database migration definitions
$forwardMigrationVersions =
    [
        '0.2.2' => [],
        '0.2.3' =>
            [
                'ALTER TABLE app_log ADD COLUMN dateTimeDelta INT NOT NULL AFTER appDateTime'
            ]
    ];
$backwardMigrationVersions =
    [
        '0.2.3' =>
            [
                'ALTER TABLE app_log DROP COLUMN dateTimeDelta'
            ],
        '0.2.2' => []
    ];

//
// HAL database services
//
// dbAddPerformanceRecord()
//   Creates a HAL service performance record and returns SUCCESS
//     OR
//   Returns an ErrorHAL object if a database access exception occurs
//
function dbAddPerformanceRecord(PDO $db, $statisticsDate, $serviceName, $elapsedTime)
{
    // Add the student_test_assignment record
    $sql = "INSERT INTO performance " .
        "(statisticsDate, " .
        "serviceName, " .
        "serviceCount, " .
        "totalElapsedTime, " .
        "maximumElapsedTime, " .
        "minimumElapsedTime) " .
        "VALUES " .
        "('$statisticsDate', " .
        "'$serviceName', " .
        "'1', " .
        "'$elapsedTime', " .
        "'$elapsedTime', " .
        "'$elapsedTime')";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
    }
    catch(PDOException $error) {
        return errorQueryException($error, __FUNCTION__, $sql);
    }

    return SUCCESS;
}

// dbConnect()
//   Returns a PDO object
//     OR
//   Returns an ErrorLDR object if an error occurs
//
function dbConnect()
{
    // Open the database server connection
    $dbHostname = DB_HOSTNAME;
    $dsn = "mysql:host=$dbHostname";
    try	{
        $pdo = new PDO($dsn, DB_USERNAME, DB_PASSWORD);
        $pdo->exec("SET NAMES utf8");
    }
    catch(PDOException $error) {
        $errorHAL = new ErrorHAL(HAL_EC_DATABASE_CONNECT_FAILED, $error->getMessage());
        $errorHAL->logError(__FUNCTION__);
        $errorHAL->logMessage(__FUNCTION__, 'DSN = |' . $dsn . '|');
        return $errorHAL;
    }

    // Configure the connection to throw exceptions
    if (!$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION))
    {
        $pdo = null; // Close the database connection
        $errorHAL = new ErrorHAL(HAL_EC_DATABASE_SET_ATTRIBUTE_FAILED,
                                 'Database connection configuration failed');
        $errorHAL->logError(__FUNCTION__);
        return $errorHAL;
    }

    // Select the LDR database
    $dbName = DB_NAME;
    try {
        $pdo->exec("USE $dbName;");
    }
    catch(PDOException $error) {
        $errorHAL = new ErrorHAL(HAL_EC_USE_DATABASE_FAILED, $error->getMessage());
        $errorHAL->logError(__FUNCTION__);
        return $errorHAL;
    }

    // Return the connection
    return $pdo;
}

// dbCreateLogEntry()
//   Creates a log entry
//     OR
//   Returns an ErrorLDR object if an error occurs
//
function dbCreateLogEntry(PDO $db, $appType, $hostname, $appDateTime, $severity, $eventType, $eventDesc)
{
    // Escape apostrophes and limit field lengths
    $appType = substr($appType, 0, LENGTH_APP_TYPE);
    $hostname = substr($hostname, 0, MAXLEN_HOSTNAME);
    $eventDesc = preg_replace("/'/", "''", $eventDesc);
    $eventDesc = substr($eventDesc, 0, MAXLEN_EVENT_DESC);

    // Generate the log timestamp
    $logDateTime = date("Y-m-d H:i:s");

    // Measure the discrepancy between the logging application's time and HAL's time
    $appTimeStamp = strtotime($appDateTime);
    if ($appTimeStamp === false)
    {
        $appDateTime = $logDateTime;
        $dateTimeDelta = -666666;
    }
    else
    {
        $logTimeStamp = strtotime($logDateTime);
        $dateTimeDelta = $logTimeStamp - $appTimeStamp;

        // Optionally sanity-check the discrepancy
        if (MAX_DATETIME_DELTA > 0)
        {
            if (abs($dateTimeDelta) > MAX_DATETIME_DELTA)
                $appDateTime = $logDateTime;
        }
    }

    // Create the app_log record
    // TODO: RESOLVE ISSUE WITH PDO PREPARED STATEMENT WHERE IT CONVERTS eventDesc ' TO ''
    // TODO: WHEN eventDesc IS PASSED SEPARATELY IN AN ARRAY TO THE execute() METHOD
    $sql = "INSERT INTO app_log (logDateTime, appDateTime, dateTimeDelta, severity,
                                 appType, hostname, eventType, eventDesc)
            VALUES ('$logDateTime', '$appDateTime', '$dateTimeDelta', '$severity',
                    '$appType', '$hostname', '$eventType', :eventDesc)";
    try {
        $stmt = $db->prepare($sql);
//        $eventDesc = $db->quote($eventDesc, PDO::PARAM_STR);
//        $eventDesc = substr($eventDesc, 1, -1);
        $stmt->execute(array(':eventDesc' => $eventDesc));
        $appLogId = $db->lastInsertId();
        return $appLogId;
    }
    catch(PDOException $error) {
        return errorQueryException($error, __FUNCTION__, $sql);
    }
}

// dbCreateNotificationLogEntry()
//   Creates a notification log entry
//     OR
//   Returns an ErrorLDR object if an error occurs
//
function dbCreateNotificationLogEntry(PDO $db, $notificationRuleId, $appLogId, $recipient)
{
    // Escape apostrophes and limit the field length
    $dbRecipient = preg_replace("/'/", "''", $recipient);
    if (strlen($dbRecipient) > MAXLEN_RECIPIENT)
        $dbRecipient = substr($dbRecipient, 0, MAXLEN_RECIPIENT);

    // Generate the log timestamp
    $logDateTime = date("Y-m-d H:i:s");

    // Create the notification_log record
    $sql = "INSERT INTO notification_log (logDateTime, notificationRuleId, appLogId, recipient)
            VALUES ('$logDateTime', '$notificationRuleId', '$appLogId', '$dbRecipient')";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        return SUCCESS;
    }
    catch(PDOException $error) {
        return errorQueryException($error, __FUNCTION__, $sql);
    }
}

// dbCreateNotificationRecipient()
//   Creates a notification recipient
//     OR
//   Returns an ErrorLDR object if an error occurs
//
function dbCreateNotificationRecipient(PDO $db, $notificationRuleId, $recipient)
{
    // Escape apostrophes and limit the field length
    $dbRecipient = preg_replace("/'/", "''", $recipient);
    if (strlen($dbRecipient) > MAXLEN_RECIPIENT)
        $dbRecipient = substr($dbRecipient, 0, MAXLEN_RECIPIENT);

    // Create the notification_list record
    $sql = "INSERT INTO notification_list (notificationRuleId, recipient)
            VALUES ('$notificationRuleId', '$dbRecipient')";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        return SUCCESS;
    }
    catch(PDOException $error) {
        return errorQueryException($error, __FUNCTION__, $sql);
    }
}

// dbCreateNotificationRule()
//   Creates a notification rule
//     OR
//   Returns an ErrorLDR object if an error occurs
//
function dbCreateNotificationRule(PDO $db, $appType, $severity, $eventType)
{
    // Escape apostrophes and limit field lengths
    if (strlen($appType) > LENGTH_APP_TYPE)
        $appType = substr($appType, 0, LENGTH_APP_TYPE);
    if (strlen($severity) > LENGTH_SEVERITY)
        $severity = substr($severity, 0, LENGTH_SEVERITY);
    if (strlen($eventType) > LENGTH_EVENT_TYPE)
        $eventType = substr($eventType, 0, LENGTH_EVENT_TYPE);

    // Create the notification_rule record
    $sql = "INSERT INTO notification_rule (appType, severity, eventType)
            VALUES ('$appType', '$severity', '$eventType')";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        return SUCCESS;
    }
    catch(PDOException $error) {
        return errorQueryException($error, __FUNCTION__, $sql);
    }
}

// dbCreateSessionToken()
//   Creates a session_token record and returns SUCCESS
//     OR
//   Returns an ErrorLDR object if a database access exception occurs
//
function dbCreateSessionToken(PDO $db, $clientId, $token)
{
    // TODO: IMPLEMENT SERVICE INVOKED BY CRON JOB WHICH REMOVES EXPIRED RECORDS

    $sql = "INSERT INTO session_token (clientId, token, dateTime)
            VALUES ('$clientId', '$token', '" . date("Y-m-d H:i:s") . "')";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
    }
    catch(PDOException $error) {
        return errorQueryException($error, __FUNCTION__, $sql);
    }

    return SUCCESS;
}

// dbCreateTable
//   Creates a HAL database table and returns SUCCESS
//     OR
//   Returns an ErrorHAL object if a database access exception occurs
//
function dbCreateTable(PDO $db, $tableName)
{
    global $dbTables;

    // Check that the table is known
    if (!array_key_exists($tableName, $dbTables))
    {
        $errorLDR = new ErrorHAL(HAL_EC_DATABASE_TABLE_UNKNOWN_TABLE,
                                 HAL_ED_DATABASE_TABLE_UNKNOWN_TABLE . $tableName);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    // Create the table
    $sql = "CREATE TABLE IF NOT EXISTS " . $tableName . " " . $dbTables[$tableName]['create'] .
           " ENGINE=InnoDB COLLATE=utf8_bin";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
    }
    catch(PDOException $error) {
        return errorQueryException($error, __FUNCTION__, $sql);
    }

    // Create built-in records
    switch ($tableName)
    {
        case 'client':
            // Add the user accounts
            $sql = "INSERT INTO client (clientName, clientPassword) VALUES " .
                "('ldr_root', 'BF5B500768FF23D0F48BE873467B94161CBEEFF4BC0FCA8BDFAEEB67C3A6812A3D074475E2B2445E35E5F19E12C42176E9A9CDC1EC5142478986CC807C027F69'), " .
                "('parcc_adp', '9B19FBEF3B836B33B7EC9627F76C7F0D0056A30F18BD6E17E13BB37748A1DE54DC5BA113E45B16A57121A4BD123ABEDF7FF40731FA7859E86360313A447BD092'), " .
                "('parcc_ci', 'F6C3BC93F9D78351CDF404098021E5CAC8F001974954A6095CA8B8823B6BBA319ABEEEB314E4A2E85373275D05800B48690D99EB5C6262A778E79DCF25025128'), " .
                "('parcc_dev', '94693C2FD7FB62797298EAC54A0FF8DC29F568ED67989C03B7B432292A1CC10A309DC398729F015BCDACBB61A577C144E3D51E325A3DD64D6EBE730C8A7F2161'), " .
                "('parcc_qa', '81D0C0189A9D6A494E8EB53598D04447DA89907E60FB33BAA545BAB744DE78AE4E4E30C0CAE278F4547DCFFD60D7820EDCC9D942A711FA2F71520058322F16C5'), " .
                "('parcc_tap', '927AE4001EBD15026831839536260745E2C0F8CD417702BB47B565AFED2A38F0B5CA787DC004A6E9AFACB1113AB94945943D95BCC1D37BCA6617231AA93D5C16'), " .
                "('parcc_user', 'A7E012D819B0EDD6E6A52F10796F93FDC80E9F7F7B86A18110E2BDD5494D1D9442F885FC845701D48FCE5F5C8432C102B4817C96B98F499EA93DD6ED88364B66')";
            try {
                $stmt = $db->prepare($sql);
                $stmt->execute();
            }
            catch(PDOException $error) {
                return errorQueryException($error, __FUNCTION__, $sql);
            }
            break;
        case 'version_history':
            // Update the database version
            $result = dbUpdateDatabaseVersion($db);
            if ($result instanceof ErrorHAL)
            {
                $result->logError(__FUNCTION__);
                return $result;
            }
            break;
    }

    return SUCCESS;
}

// dbDeleteNotificationRecipient()
//   Deletes a notification recipient and returns SUCCESS
//     OR
//   Returns an ErrorHAL object if a database access exception occurs
//
function dbDeleteNotificationRecipient($db, $notificationListId)
{
    $sql = "UPDATE notification_list SET deleted = 'Y' WHERE notificationListId = '$notificationListId'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
    }
    catch(PDOException $error) {
        return errorQueryException($error, __FUNCTION__, $sql);
    }

    return SUCCESS;
}

// dbDeleteNotificationRule()
//   Deletes a notification rule and all recipients and returns SUCCESS
//     OR
//   Returns an ErrorHAL object if a database access exception occurs
//
function dbDeleteNotificationRule($db, $notificationRuleId)
{
    // Delete the notification rule recipients
    $sql = "UPDATE notification_list SET deleted = 'Y' WHERE notificationRuleId = '$notificationRuleId'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
    }
    catch(PDOException $error) {
        return errorQueryException($error, __FUNCTION__, $sql);
    }

    // Delete the notification rule
    $sql = "UPDATE notification_rule SET deleted = 'Y' WHERE notificationRuleId = '$notificationRuleId'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
    }
    catch(PDOException $error) {
        return errorQueryException($error, __FUNCTION__, $sql);
    }

    return SUCCESS;
}

// dbDeleteSessionToken()
//   Deletes a session_token record and returns SUCCESS
//     OR
//   Returns an ErrorHAL object if a database access exception occurs
//
function dbDeleteSessionToken(PDO $db, $token)
{
    $sql = "DELETE FROM session_token WHERE token = '$token'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
    }
    catch(PDOException $error) {
        return errorQueryException($error, __FUNCTION__, $sql);
    }

    return SUCCESS;
}

// dbDropAllTables()
//   Drops all tables except the session_token table from the HAL database and returns SUCCESS
//     OR
//   Returns an ErrorLDR object if an error occurs
//
function dbDropAllTables(PDO $db)
{
    // Delete tables ordered by dependencies such as foreign key constraints
    $sql = "DROP TABLE IF EXISTS
            notification_log,
            notification_list,
            notification_rule,
            performance,
            app_log,
            client,
            version_history";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
    }
    catch(PDOException $error) {
        return errorQueryException($error, __FUNCTION__, $sql);
    }

    return SUCCESS;
}

// dbDropTable()
//   Drops a table from the LDR database and returns SUCCESS
//     OR
//   Returns an ErrorLDR object if an error occurs
//
function dbDropTable(PDO $db, $tableName)
{
    global $dbTables;

    // Check that the table is known
    if (!array_key_exists($tableName, $dbTables))
    {
        $errorLDR = new ErrorHAL(HAL_EC_DATABASE_TABLE_UNKNOWN_TABLE,
                                 HAL_ED_DATABASE_TABLE_UNKNOWN_TABLE . $tableName);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    $sql = "DROP TABLE IF EXISTS " . $tableName;
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
    }
    catch(PDOException $error) {
        return errorQueryException($error, __FUNCTION__, $sql);
    }

    return SUCCESS;
}

// dbGetDatabaseVersion()
//   Returns the database version if found, null otherwise
//     OR
//   Returns an ErrorLDR object if there is an error
//
function dbGetDatabaseVersion(PDO $db)
{
    // Get the last entry
    $sql = "SELECT versionHistoryId, version FROM version_history
            ORDER BY versionHistoryId DESC LIMIT 1";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() == 1)
        {
            $row = $stmt->fetch(PDO::FETCH_NUM);
            return $row[1];
        }
    }
    catch(PDOException $error) {
        return errorQueryException($error, __FUNCTION__, $sql);
    }

    return null; // Version not found
}

// dbGetLogEntries()
//   Returns an array of log entries for a given date range
//     OR
//   Returns an ErrorLDR object if a database access exception occurs
//
function dbGetLogEntries(PDO $db, $startDateTime, $endDateTime, $appType, $hostname, $severity, $eventType1,
                         $eventType2, $appLogId, $pageOffset = 0, $pageLimit = DEFAULT_PAGE_LIMIT)
{
    // Create the base query
    $sql = "SELECT appLogId, appDateTime, appType, severity, eventType, hostname, eventDesc
            FROM app_log WHERE TRUE";

    // Append optional query modifiers
    if (strlen($startDateTime) > 0)
        $sql .= " AND appDateTime >= '$startDateTime'";
    if (strlen($endDateTime) > 0)
        $sql .= " AND appDateTime <= '$endDateTime'";
    if (strlen($appType) == LENGTH_APP_TYPE)
        $sql .= " AND appType = '$appType'";
    if (strlen($hostname) > 0)
        $sql .= " AND hostname = '$hostname'";
    if (strlen($severity) > 0)
        $sql .= " AND severity = '$severity'";
    if (strlen($eventType1) > 0 && strlen($eventType2) > 0)
        $sql .= " AND (eventType = '$eventType1' OR eventType = '$eventType2')";
    elseif (strlen($eventType1) > 0)
        $sql .= " AND eventType = '$eventType1'";
    if (strlen($appLogId) > 0)
        $sql .= " AND appLogId = '$appLogId'";

    // Sort the log entries by appDateTime
    $sql .= " ORDER BY appDateTime";

    // Set the page offset and limit
    $sql .= " LIMIT " . $pageOffset . ', ' . $pageLimit;

    // Get the app_log records
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $records = $stmt->fetchAll(PDO::FETCH_ASSOC);

        // Fix the single quotes in the event description
        foreach ($records as $key => $value)
            $records[$key]['eventDesc'] = str_replace("''", "'", $records[$key]['eventDesc']);

        return $records;
    }
    catch(PDOException $error) {
        return errorQueryException($error, __FUNCTION__, $sql);
    }
}

// dbGetMatchingNotificationRules()
//   Returns an array of notification rule IDs
//     OR
//   Returns an ErrorLDR object if a database access exception occurs
//
function dbGetMatchingNotificationRules(PDO $db, $appType, $severity, $eventType)
{
    // Create the base query
    $sql = "SELECT notificationRuleId FROM notification_rule WHERE deleted = 'N' AND (";

    // Append the matching qualifiers
    $sql .=    "(appType = '$appType' AND severity = '' AND eventType = '')";

    $sql .= "OR (appType = '' AND severity = '$severity' AND eventType = '')";

    $sql .= "OR (appType = '$appType' AND severity = '$severity' AND eventType = '')";

    $sql .= "OR (appType = '' AND severity = '' AND eventType = '$eventType')";

    $sql .= "OR (appType = '$appType' AND severity = '' AND eventType = '$eventType')";

    $sql .= "OR (appType = '' AND severity = '$severity' AND eventType = '$eventType')";

    $sql .= "OR (appType = '$appType' AND severity = '$severity' AND eventType = '$eventType')";

    $sql .= ")";

    // Get the notification_rule records
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_NUM);
        $notificationRuleIds = [];
        foreach ($rows as $row)
            $notificationRuleIds[] = $row[0];
        return $notificationRuleIds;
    }
    catch(PDOException $error) {
        return errorQueryException($error, __FUNCTION__, $sql);
    }
}

// dbGetNotificationList()
//   Returns an array of notification recipients
//     OR
//   Returns an ErrorLDR object if a database access exception occurs
//
function dbGetNotificationList($db, $notificationRuleId, $recipient)
{
    // Create the base query
    $sql = "SELECT nl.notificationListId, nl.notificationRuleId, nr.appType, nr.severity, nr.eventType, nl.recipient
            FROM notification_list nl
            JOIN notification_rule nr on nr.notificationRuleId = nl.notificationRuleId
            WHERE nl.deleted = 'N'
            AND nr.deleted = 'N'";

    // Append optional query modifiers
    if (strlen($notificationRuleId) > 0)
        $sql .= " AND nl.notificationRuleId = '$notificationRuleId'";
    if (strlen($recipient) > 0)
    {
        // Escape apostrophes and limit the field length
        $dbRecipient = preg_replace("/'/", "''", $recipient);
        if (strlen($dbRecipient) > MAXLEN_RECIPIENT)
            $dbRecipient = substr($dbRecipient, 0, MAXLEN_RECIPIENT);
        $sql .= " AND nl.recipient LIKE '%$dbRecipient%'";
    }

    // Sort the log entries by appDateTime
    $sql .= " ORDER BY nl.notificationRuleId, nl.recipient";

    // Get the notification_list records
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $records = $stmt->fetchAll(PDO::FETCH_OBJ);
        return $records;
    }
    catch(PDOException $error) {
        return errorQueryException($error, __FUNCTION__, $sql);
    }
}

// dbGetNotificationListId()
//   Returns a notificationListId or null if the notification recipient is not found
//     OR
//   Returns an ErrorLDR object if a database access exception occurs
//
function dbGetNotificationListId(PDO $db, $notificationRuleId, $recipient)
{
    // Escape apostrophes and limit the field length
    $dbRecipient = preg_replace("/'/", "''", $recipient);
    if (strlen($dbRecipient) > MAXLEN_RECIPIENT)
        $dbRecipient = substr($dbRecipient, 0, MAXLEN_RECIPIENT);

    $sql = "SELECT notificationListId
            FROM notification_list
            WHERE notificationRuleId = '$notificationRuleId'
            AND recipient = '$dbRecipient'
            AND deleted = 'N'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() == 1)
        {
            $row = $stmt->fetch(PDO::FETCH_NUM);
            if (strlen($row[0]) > 0)
                return $row[0];
        }
    }
    catch(PDOException $error) {
        return errorQueryException($error, __FUNCTION__, $sql);
    }

    return null; // Notification recipient not found
}

// dbGetNotificationLogEntries()
//   Returns an array of notification log entries for a given date range
//     OR
//   Returns an ErrorLDR object if a database access exception occurs
//
function dbGetNotificationLogEntries(PDO $db, $startDateTime, $endDateTime, $appLogId, $notificationRuleId, $recipient,
                                     $pageOffset = 0, $pageLimit = DEFAULT_PAGE_LIMIT)
{
    // Create the base query
    $sql = "SELECT notificationLogId, logDateTime, notificationRuleId, appLogId, recipient
            FROM notification_log WHERE TRUE";

    // Append optional query modifiers
    if (strlen($startDateTime) > 0)
        $sql .= " AND logDateTime >= '$startDateTime'";
    if (strlen($endDateTime) > 0)
        $sql .= " AND logDateTime <= '$endDateTime'";
    if (strlen($appLogId) > 0)
        $sql .= " AND appLogId = '$appLogId'";
    if (strlen($appLogId) > 0)
        $sql .= " AND appLogId = '$appLogId'";
    if (strlen($notificationRuleId) > 0)
        $sql .= " AND notificationRuleId = '$notificationRuleId'";
    if (strlen($recipient) > 0)
    {
        // Escape apostrophes and limit the field length
        $dbRecipient = preg_replace("/'/", "''", $recipient);
        if (strlen($dbRecipient) > MAXLEN_RECIPIENT)
            $dbRecipient = substr($dbRecipient, 0, MAXLEN_RECIPIENT);
        $sql .= " AND recipient LIKE '%$dbRecipient%'";
    }

    // Sort the log entries by logDateTime
    $sql .= " ORDER BY logDateTime";

    // Set the page offset and limit
    $sql .= " LIMIT " . $pageOffset . ', ' . $pageLimit;

    // Get the notification_log records
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $records = $stmt->fetchAll(PDO::FETCH_OBJ);
        return $records;
    }
    catch(PDOException $error) {
        return errorQueryException($error, __FUNCTION__, $sql);
    }
}

// dbGetNotificationRuleId()
//   Returns a notificationRuleId or null if the rule is not found
//     OR
//   Returns an ErrorLDR object if a database access exception occurs
//
function dbGetNotificationRuleId(PDO $db, $appType, $severity, $eventType)
{
    $sql = "SELECT notificationRuleId
            FROM notification_rule
            WHERE appType = '$appType'
            AND severity = '$severity'
            AND eventType = '$eventType'
            AND deleted = 'N'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() == 1)
        {
            $row = $stmt->fetch(PDO::FETCH_NUM);
            if (strlen($row[0]) > 0)
                return $row[0];
        }
    }
    catch(PDOException $error) {
        return errorQueryException($error, __FUNCTION__, $sql);
    }

    return null; // Notification rule not found
}

// dbGetNotificationRuleRecipients()
//   Returns an array of the notification recipients for a notification rule
//     OR
//   Returns an ErrorLDR object if a database access exception occurs
//
function dbGetNotificationRuleRecipients(PDO $db, $notificationRuleId)
{
    $sql = "SELECT recipient FROM notification_list
            WHERE deleted = 'N'
            AND notificationRuleId = '$notificationRuleId'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_NUM);
        $recipients = [];
        foreach ($rows as $row)
            $recipients[] = $row[0];
        return $recipients;
    }
    catch(PDOException $error) {
        return errorQueryException($error, __FUNCTION__, $sql);
    }
}

// dbGetNotificationRules()
//   Returns an array of notification rules
//     OR
//   Returns an ErrorLDR object if a database access exception occurs
//
function dbGetNotificationRules(PDO $db, $appType, $severity, $eventType)
{
    // Create the base query
    $sql = "SELECT notificationRuleId, appType, severity, eventType
            FROM notification_rule
            WHERE deleted = 'N'";

    // Append optional query modifiers
    if (strlen($appType) == LENGTH_APP_TYPE)
        $sql .= " AND appType = '$appType'";
    if (strlen($severity) > 0)
        $sql .= " AND severity = '$severity'";
    if (strlen($eventType) > 0)
        $sql .= " AND eventType = '$eventType'";

    // Sort the log entries by appDateTime
    $sql .= " ORDER BY notificationRuleId";

    // Get the notification_rule records
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $records = $stmt->fetchAll(PDO::FETCH_OBJ);
        return $records;
    }
    catch(PDOException $error) {
        return errorQueryException($error, __FUNCTION__, $sql);
    }
}

// dbGetPerformanceRecord()
//   Returns values from a HAL service performance record
//     OR
//   Returns an ErrorHAL object if a database access exception occurs
//
function dbGetPerformanceRecord(PDO $db, $statisticsDate, $serviceName)
{
    $performanceData = null;

    $sql = "SELECT performanceId, serviceCount, totalElapsedTime, maximumElapsedTime, minimumElapsedTime
            FROM performance WHERE statisticsDate = '$statisticsDate' AND serviceName = '$serviceName'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() == 1)
        {
            $performanceData = $stmt->fetch(PDO::FETCH_ASSOC);
        }
    }
    catch(PDOException $error) {
        return errorQueryException($error, __FUNCTION__, $sql);
    }

    return $performanceData;
}

// dbGetPerformanceStatistics()
//   Returns an array of performance statistics for a given date range
//     OR
//   Returns an Errorhal object if a database access exception occurs
//
function dbGetPerformanceStatistics(PDO $db, $startDate, $endDate)
{
    // Get the performance records
    $sql = "SELECT serviceName,
                   serviceCount,
                   totalElapsedTime,
                   maximumElapsedTime,
                   minimumElapsedTime
            FROM performance
            WHERE statisticsDate >= '$startDate'
            AND statisticsDate <= '$endDate'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $records = $stmt->fetchAll(PDO::FETCH_OBJ);
    }
    catch(PDOException $error) {
        return errorQueryException($error, __FUNCTION__, $sql);
    }

    // Aggregate the performance data by serviceName
    $services = [];
    foreach ($records as $record)
    {
        // Get this service from the array or add it
        if (array_key_exists($record->serviceName, $services))
            $service = $services[$record->serviceName];
        else
        {
            $service = new Performance();
            $services[$record->serviceName] = $service;
        }

        // Aggregate the values for this service
        $service->serviceCount += $record->serviceCount;
        $service->totalElapsedTime += $record->totalElapsedTime;
        if ($service->maximumElapsedTime < $record->maximumElapsedTime)
            $service->maximumElapsedTime = $record->maximumElapsedTime;
        if ($service->minimumElapsedTime > $record->minimumElapsedTime)
            $service->minimumElapsedTime = $record->minimumElapsedTime;
    }

    // Calculate the average elapsed time for each service
    foreach ($services as $service)
        $service->averageElapsedTime = $service->totalElapsedTime / $service->serviceCount;

    // Sort the services by name
    ksort($services);

    return $services;
}

// dbGetSessionDatetime_By_Token()
//   Returns a session datetime if found, null otherwise
//     OR
//   Returns an ErrorLDR object if there is an error
//
function dbGetSessionDatetime_By_Token(PDO $db, $token)
{
    // Get the session token timestamp
    $sql = "SELECT dateTime FROM session_token WHERE token = '$token'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() == 1)
        {
            $row = $stmt->fetch(PDO::FETCH_NUM);
            if (strlen($row[0]) > 0)
                return $row[0];
        }
    }
    catch(PDOException $error) {
        return errorQueryException($error, __FUNCTION__, $sql);
    }

    return null; // Token not found
}

// dbGetTableNames()
//   Returns an array of the LDR table names
//     OR
//   Returns an ErrorLDR object if an error occurs
//
function dbGetTableNames(PDO $db)
{
    // Get the table names
    $sql = "SHOW TABLES";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $rows = $stmt->fetchAll(PDO::FETCH_NUM);
    }
    catch(PDOException $error) {
        return errorQueryException($error, __FUNCTION__, $sql);
    }

    // Extract the table names
    $tables = [];
    foreach ($rows as $row)
        $tables[] = $row[0];

    return $tables;
}

// dbMigrateDatabase()
//   Migrates a HAL database to the required version and returns SUCCESS
//     OR
//   Returns an ErrorHAL object if an error occurs
//
function dbMigrateDatabase(PDO $db)
{
    // Get the database version
    $databaseVersion = dbGetDatabaseVersion($db);
    if ($databaseVersion instanceof ErrorHAL)
    {
        $databaseVersion->logError(__FUNCTION__);
        return $databaseVersion;
    }

    // Determine the migration type
    if ($databaseVersion < HAL_DATABASE_VERSION)
        $result = dbMigrateDatabaseForward($db, $databaseVersion);
    elseif ($databaseVersion > HAL_DATABASE_VERSION)
        $result = dbMigrateDatabaseBackward($db, $databaseVersion);
    else
        $result = SUCCESS;

    return $result;
}

// dbMigrateDatabaseBackward()
//   Migrates a HAL database backward to the required version and returns SUCCESS
//     OR
//   Returns an ErrorHAL object if an error occurs
//
function dbMigrateDatabaseBackward(PDO $db, $databaseVersion)
{
    global $backwardMigrationVersions;

    // Check the backward migration versions
    foreach ($backwardMigrationVersions as $migrationVersion => $migrationCommands)
    {
        // Skip all backward migration versions down to and including the current database version
        if ($migrationVersion > $databaseVersion)
            continue;

        // Process subsequent backward migration versions down to but excluding the required database version
        if ($migrationVersion > HAL_DATABASE_VERSION)
        {
            foreach ($migrationCommands as $migrationCommand)
            {
                // Execute the migration command
                try {
                    $stmt = $db->prepare($migrationCommand);
                    $stmt->execute();
                }
                catch(PDOException $error) {
                    return errorQueryException($error, __FUNCTION__, $migrationCommand);
                }
            }
        }
        else
        {
            // If we are at the required database version then we are done
            break;
        }
    }

    // Update the database version
    $result = dbUpdateDatabaseVersion($db);
    if ($result instanceof ErrorHAL)
    {
        $result->logError(__FUNCTION__);
        return $result;
    }

    return SUCCESS;
}

// dbMigrateDatabaseForward()
//   Migrates a HAL database forward to the required version and returns SUCCESS
//     OR
//   Returns an ErrorHAL object if an error occurs
//
function dbMigrateDatabaseForward(PDO $db, $databaseVersion)
{
    global $forwardMigrationVersions;

    // Check the forward migration versions
    foreach ($forwardMigrationVersions as $migrationVersion => $migrationCommands)
    {
        // Skip all forward migration versions up to and including the current database version
        if ($migrationVersion <= $databaseVersion)
            continue;

        // Process subsequent forward migration versions up to and including the required database version
        if ($migrationVersion <= HAL_DATABASE_VERSION)
        {
            foreach ($migrationCommands as $migrationCommand)
            {
                // Execute the migration command
                try {
                    $stmt = $db->prepare($migrationCommand);
                    $stmt->execute();
                }
                catch(PDOException $error) {
                    return errorQueryException($error, __FUNCTION__, $migrationCommand);
                }
            }
        }
        else
        {
            // If we are above the required database version then we are done
            break;
        }
    }

    // Update the database version
    $result = dbUpdateDatabaseVersion($db);
    if ($result instanceof ErrorHAL)
    {
        $result->logError(__FUNCTION__);
        return $result;
    }

    return SUCCESS;
}

// dbRecordPerformance()
//   Records the elapsed time performance for an LDR service call
//     OR
//   Logs any errors and fails silently
//
function dbRecordPerformance(PDO $db, $statisticsDate, $serviceName, $elapsedTime)
{
    // Check if a performance record exists
    $performanceData = dbGetPerformanceRecord($db, $statisticsDate, $serviceName);
    if ($performanceData instanceof ErrorHAL)
    {
        $performanceData->logError(__FUNCTION__);
        return;
    }
    if (is_null($performanceData))
    {
        // Performance record does not exist so create it
        $result = dbAddPerformanceRecord($db, $statisticsDate, $serviceName, $elapsedTime);
        if ($result instanceof ErrorHAL)
        {
            $result->logError(__FUNCTION__);
            return;
        }
    }
    else
    {
        // Performance record exists so update it
        $performanceData['serviceCount']++;
        $performanceData['totalElapsedTime'] += $elapsedTime;
        if ($performanceData['maximumElapsedTime'] < $elapsedTime)
            $performanceData['maximumElapsedTime'] = $elapsedTime;
        if ($performanceData['minimumElapsedTime'] > $elapsedTime)
            $performanceData['minimumElapsedTime'] = $elapsedTime;

        $result = dbUpdatePerformanceRecord($db, $performanceData['performanceId'],
            $performanceData['serviceCount'], $performanceData['totalElapsedTime'],
            $performanceData['maximumElapsedTime'], $performanceData['minimumElapsedTime']);
        if ($result instanceof ErrorHAL)
        {
            $result->logError(__FUNCTION__);
            return;
        }
    }
}

// dbTableValueExists()
//   Returns true if a value exists, false otherwise
//     OR
//   Returns an ErrorLDR object if an error occurs
//
function dbTableValueExists(PDO $db, $tableName, $columnName, $value)
{
    $valueExists = false;

    $sql = "SELECT COUNT(*) FROM $tableName WHERE $columnName = '$value' AND deleted = 'N'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        if ($row[0] > 0)
            $valueExists = true;
    }
    catch(PDOException $error) {
        return errorQueryException($error, __FUNCTION__, $sql);
    }

    return $valueExists;
}

// dbTouchSessionToken()
//   Makes a session_token dateTime value current and returns SUCCESS
//     OR
//   Returns an ErrorLDR object if a database access exception occurs
//
function dbTouchSessionToken(PDO $db, $token)
{
    // Make the session token timestamp current
    $sql = "UPDATE session_token SET dateTime = '" . date("Y-m-d H:i:s") . "' WHERE token = '$token'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
    }
    catch(PDOException $error) {
        return errorQueryException($error, __FUNCTION__, $sql);
    }

    return SUCCESS;
}

// dbUpdateDatabaseVersion()
//   Creates a database_version record and returns SUCCESS
//     OR
//   Returns an ErrorLDR object if an error occurs
//
function dbUpdateDatabaseVersion(PDO $db)
{
    // Add the current database version
    $sql = "INSERT INTO version_history (version, dateTime)
            VALUES ('" . HAL_DATABASE_VERSION . "', '" . date("Y-m-d H:i:s") . "')";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
    }
    catch(PDOException $error) {
        return errorQueryException($error, __FUNCTION__, $sql);
    }

    return SUCCESS;
}

// dbUpdatePerformanceRecord()
//   Updates a HAL service performance record and returns SUCCESS
//     OR
//   Returns an ErrorHAL object if a database access exception occurs
//
function dbUpdatePerformanceRecord(PDO $db, $performanceId, $serviceCount, $totalElapsedTime, $maximumElapsedTime,
    $minimumElapsedTime)
{
    $sql = "UPDATE performance SET " .
        "serviceCount = '$serviceCount', " .
        "totalElapsedTime = '$totalElapsedTime', " .
        "maximumElapsedTime = '$maximumElapsedTime', " .
        "minimumElapsedTime = '$minimumElapsedTime' " .
        "WHERE performanceId = '$performanceId'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
    }
    catch(PDOException $error) {
        return errorQueryException($error, __FUNCTION__, $sql);
    }

    return SUCCESS;
}

// errorQueryException()
//   Logs a database query exception and returns an ErrorHAL object
//
function errorQueryException(PDOException $error, $functionName, $sql)
{
	$errorHAL = new ErrorHAL(HAL_EC_QUERY_EXCEPTION, $error->getMessage());
	$errorHAL->logError($functionName);
	$errorHAL->logMessage($functionName, 'SQL = |' . $sql . '|');
	return $errorHAL;
}
