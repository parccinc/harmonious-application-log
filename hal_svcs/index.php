<?php

require 'vendor/autoload.php';	// Import Composer-managed packages (PHPMailer, etc.)

// These primary includes must be first and in this order
require_once '../hal_common/definitions.php';
require_once 'config.php';
require_once 'Slim/Slim.php';

// These secondary includes can be in any order
require_once 'database.php';
require_once 'validation.php';

define('HAL_SOFTWARE_VERSION', '0.4.1');
define('HAL_COMMIT_DATE', '2016-03-28');

//
// PARCC-HAL Service Functions Index
//
// createAllTables() ................................... 2014-11-16 refactored
//  create_all_tables.php
//
// createLogEntry() .................................... 2014-12-02 implemented
//  create_log_entry.php
//
// createNotificationRecipient() ....................... 2015-03-17 implemented
//  create_notification_recipient.php
//
// createNotificationRule() ............................ 2015-03-13 implemented
//  create_notification_rule.php
//
// createSessionToken() ................................ 2015-03-12 added clientId
//  index.php => login_user.php
//
// createTable() ....................................... 2014-11-16 refactored
//  create_table.php
//
// deleteNotificationRecipient() ....................... 2015-03-18 implemented
//  delete_notification_recipient.php
//
// deleteNotificationRule() ............................ 2015-03-20 implemented
//  delete_notification_rule.php
//
// deleteSessionToken() ................................ 2014-11-17 refactored
//  logout.php
//
// dropAllTables() ..................................... 2014-11-20 refactored
//  drop_all_tables.php
//
// dropTable() ......................................... 2014-12-02 refactored
//  drop_table.php
//
// getDatabaseVersion() ................................ 2015-04-08 implemented
//  get_database_version.php
//
// getLogEntries() ..................................... 2014-12-04 implemented
//  get_log_entries.php
//
// getNotificationList() ............................... 2015-03-18 implemented
//  get_notification_list.php
//
// getNotificationLogEntries() ......................... 2015-03-20 implemented
//  get_notification_log_entries.php
//
// getNotificationRules() .............................. 2015-03-16 implemented
//  get_notification_rules.php
//
// getPerformanceStatistics() .......................... 2014-12-11 refactored
//  get_performance_statistics.php
//
// getTableNames() ..................................... 2014-11-15 refactored
//  get_table_names.php
//
// getVersion() ........................................ 2014-11-15 refactored
//  get_version.php
//
// updateDatabase() .................................... 2015-04-08 implemented
//  update_database.php
//

//
// Initialize the HAL Slim Framework application object
//
$app = new Slim(['debug' => true, 'log.enable' => true]);
$app->setName('HAL Services');
$app->response()->header('Content-Type', 'application/json; charset=utf-8'); // Set the response content type to JSON
require_once 'routes.php'; // Configure the routing
$app->run(); // Perform the routing

//
// PARCC-HAL Service Functions
//
// createAllTables()
//   Creates the HAL database tables and returns SUCCESS as a JSON encoded value
//     OR
//   Returns a JSON encoded HAL error if there is an error
//
function createAllTables()
{
    global $dbTables;

    if (DB_DISABLE_TABLE_OPERATIONS)
    {
        // Database operations are disabled
        $errorHAL = new ErrorHAL(HAL_EC_TABLE_OPERATIONS_DISABLED, 'Table creation is disabled');
        $errorHAL->returnErrorJSON(__FUNCTION__);
        return;
    }

    // Get the database connection
    $db = dbConnect();
    if ($db instanceof ErrorHAL)
    {
        $db->returnErrorJSON(__FUNCTION__);
        return;
    }

    // Create the tables
    $result = null;
    foreach ($dbTables as $tableName => $table)
    {
        // Create the table
        $result = dbCreateTable($db, $tableName);
        if ($result instanceof ErrorHAL)
        {
            $result->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
    }

    // Return the response
    echo json_encode(['result' => $result]);

    // Close the database connection
    $db = null;
}

// createLogEntry()
//   Creates an application log entry
//     OR
//   Returns a JSON encoded HAL error if there is an error
//
function createLogEntry()
{
    $startTime = microtime(true);

    // Initialize the service
    $args = beginService(TOKEN_OPTIONAL, __FUNCTION__,
        array
        (
            'appType' => DCO_REQUIRED | DCO_TOUPPER,
            'hostname' => DCO_REQUIRED | DCO_TOLOWER,
            'appDateTime' => DCO_REQUIRED,
            'severity' => DCO_REQUIRED,
            'eventType' => DCO_REQUIRED,
            'eventDesc' => DCO_OPTIONAL
        ));
    if ($args instanceof ErrorHAL)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $appType, $hostname, $appDateTime, $severity, $eventType, $eventDesc) = $args;

    // Check the appDateTime
    $appDateTime = validateDateTime($appDateTime, 'appDateTime');
    if ($appDateTime instanceof ErrorHAL)
    {
        $appDateTime->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Check the severity
    if ($severity > MAX_SEVERITY)
    {
        $errorHAL = createValueError('severity', $severity);
        $errorHAL->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Create the log entry
    $appLogId = dbCreateLogEntry($db, $appType, $hostname, $appDateTime, $severity, $eventType, $eventDesc);
    if ($appLogId instanceof ErrorHAL)
    {
        $appLogId->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Check if this log entry has any matching notification rules
    $notificationRuleIds = dbGetMatchingNotificationRules($db, $appType, $severity, $eventType);
    if ($notificationRuleIds instanceof ErrorHAL)
    {
        $notificationRuleIds->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }
    if (sizeof($notificationRuleIds) > 0)
    {
        // Format the email message
        try {
            $template = file_get_contents('notification_template.html');
        }
        catch (Exception $e)
        {
            $errorHAL = new ErrorHAL(HAL_EC_NOTIFICATION_TEMPLATE_LOAD_FAILED,
                                     'Unable to load notification template: ' . $e->getMessage());
            $errorHAL->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
        $template = str_replace('{{ imageSrc }}', EMAIL_IMAGE_URL, $template);
        $template = str_replace('{{ imageWidth }}', EMAIL_IMAGE_WIDTH, $template);
        $template = str_replace('{{ imageHeight }}', EMAIL_IMAGE_HEIGHT, $template);
        $template = str_replace('{{ appDateTime }}', $appDateTime, $template);
        $template = str_replace('{{ hostname }}', $hostname, $template);
        $template = str_replace('{{ appType }}', $appType, $template);
        $template = str_replace('{{ severity }}', $severity, $template);
        $template = str_replace('{{ eventType }}', $eventType, $template);
        $template = str_replace('{{ eventDesc }}', $eventDesc, $template);

        // Process each matching notification rule
        foreach ($notificationRuleIds as $notificationRuleId)
        {
            // Check if the notification rule has any recipients
            $recipients = dbGetNotificationRuleRecipients($db, $notificationRuleId);
            if ($recipients instanceof ErrorHAL)
            {
                $recipients->returnErrorJSON(__FUNCTION__);
                $db = null;
                return;
            }
            if (sizeof($recipients) > 0)
            {
                // Send the notifications
                $transmit = sendNotifications(EMAIL_SUBJECT, EMAIL_FROM_ADDR, EMAIL_FROM_NAME, $recipients, $template);
                if ($transmit instanceof ErrorHAL)
                {
                    $transmit->returnErrorJSON(__FUNCTION__);
                    $db = null;
                    return;
                }

                // Log the notifications
                foreach ($recipients as $recipient)
                {
                    $nle = dbCreateNotificationLogEntry($db, $notificationRuleId, $appLogId, $recipient);
                    if ($nle instanceof ErrorHAL)
                    {
                        $nle->returnErrorJSON(__FUNCTION__);
                        $db = null;
                        return;
                    }
                }
            }
        }
    }

    // Return the result
    finishService($db, ['result' => SUCCESS], $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// createNotificationRecipient()
//   Creates a notification list entry
//     OR
//   Returns a JSON encoded HAL error if there is an error
//
function createNotificationRecipient($notificationRuleId)
{
    global $argFilterMap;

    $startTime = microtime(true);

    // Initialize the service
    $args = beginService(TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            'recipient' => DCO_REQUIRED | DCO_TOLOWER
        ));
    if ($args instanceof ErrorHAL)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $recipient) = $args;

    // Validate the notificationRuleId
    $argName = 'notificationRuleId';
    $notificationRuleId = validateArg($db, [$argName => $notificationRuleId], $argName, $argFilterMap[$argName],
                                      DCO_REQUIRED | DCO_MUST_EXIST);
    if ($notificationRuleId instanceof ErrorHAL)
    {
        $notificationRuleId->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Check that the recipient is a valid email address
    if (!filter_var($recipient, FILTER_VALIDATE_EMAIL))
    {
        $errorHAL = new ErrorHAL(HAL_EC_RECIPIENT_INVALID_FORMAT, 'Notification recipient ' . $recipient .
            ' is not a valid email format');
        $errorHAL->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Check that the notification recipient does not already exist
    $notificationListId = dbGetNotificationListId($db, $notificationRuleId, $recipient);
    if ($notificationListId instanceof ErrorHAL)
    {
        $notificationListId->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }
    elseif ($notificationListId !== null)
    {
        $errorHAL = new ErrorHAL(HAL_EC_DUPLICATE_NOTIFICATION_RECIPIENT, 'Notification rule ' . $notificationRuleId .
                                 ' is already configured with recipient ' . $recipient);
        $errorHAL->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Create the notification recipient
    $result = dbCreateNotificationRecipient($db, $notificationRuleId, $recipient);
    if ($result instanceof ErrorHAL)
    {
        $result->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the result
    finishService($db, ['result' => $result], $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// createNotificationRule()
//   Creates a notification rule
//     OR
//   Returns a JSON encoded HAL error if there is an error
//
function createNotificationRule()
{
    global $appTypes;

    $startTime = microtime(true);

    // Initialize the service
    $args = beginService(TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            'appType' => DCO_OPTIONAL | DCO_TOUPPER,
            'severity' => DCO_OPTIONAL,
            'eventType' => DCO_OPTIONAL
        ));
    if ($args instanceof ErrorHAL)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $appType, $severity, $eventType) = $args;

    // Check the appType
    if (strlen($appType) > 0 && !in_array($appType, $appTypes))
    {
        $errorLDR = new ErrorHAL(HAL_EC_APP_TYPE_UNKNOWN, HAL_ED_APP_TYPE_UNKNOWN . $appType);
        $errorLDR->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Range-check the severity
    if (strlen($severity) > 0 && $severity > MAX_SEVERITY)
    {
        $errorHAL = createValueError('severity', $severity);
        $errorHAL->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Check that there is at least one rule parameter
    if (strlen($severity) == 0 && strlen($appType) == 0&& strlen($eventType) == 0)
    {
        $errorLDR = new ErrorHAL(HAL_EC_RULE_PARAMETER_NOT_PROVIDED, HAL_ED_RULE_PARAMETER_NOT_PROVIDED);
        $errorLDR->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Check that the rule does not already exist
    $notificationRuleId = dbGetNotificationRuleId($db, $appType, $severity, $eventType);
    if ($notificationRuleId instanceof ErrorHAL)
    {
        $notificationRuleId->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }
    elseif ($notificationRuleId !== null)
    {
        $errorHAL = new ErrorHAL(HAL_EC_DUPLICATE_NOTIFICATION_RULE, HAL_ED_DUPLICATE_NOTIFICATION_RULE .
            'appType = ' . $appType . ', severity = ' . $severity . ', eventType = ' . $eventType);
        $errorHAL->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Create the notification rule
    $result = dbCreateNotificationRule($db, $appType, $severity, $eventType);
    if ($result instanceof ErrorHAL)
    {
        $result->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the result
    finishService($db, ['result' => $result], $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// createSessionToken()
//   Checks a user password and returns a session token as a JSON encoded value if the password is correct
//     OR
//   Returns a JSON encoded HAL error if there is an error or the password is incorrect
//
function createSessionToken()
{
    $startTime = microtime(true);

    // Initialize the service
    $args = beginService(TOKEN_OPTIONAL, __FUNCTION__,
        array
        (
            'clientName' => DCO_REQUIRED | DCO_TOLOWER,
            'password' => DCO_REQUIRED
        ));
    if ($args instanceof ErrorHAL)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $clientName, $password) = $args;

    // Get the clientID and clientPassword hash
    $sql = "SELECT clientId, clientPassword FROM client WHERE clientName = '" . $clientName . "'";
    try {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        if ($stmt->rowCount() == 0)
        {
            $errorLDR = new ErrorHAL(HAL_EC_CREDENTIALS_NOT_VALID, HAL_ED_CREDENTIALS_NOT_VALID);
            $errorLDR->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        $valid_password_hash = $result['clientPassword'];
        $clientId = $result['clientId'];
    }
    catch(PDOException $error) {
        errorQueryExceptionJSON($error, __FUNCTION__, $sql);
        $db = null;
        return;
    }

    // Check if the password is correct
    $password_hash = generatePasswordHash($password, $valid_password_hash);
    if ($password_hash != $valid_password_hash)
    {
        $errorLDR = new ErrorHAL(HAL_EC_CREDENTIALS_NOT_VALID, HAL_ED_CREDENTIALS_NOT_VALID);
        $errorLDR->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Create a session token
    $token = strtoupper(sha1(uniqid(rand(), true)));
    if ($token === false)
    {
        $errorLDR = new ErrorHAL(HAL_EC_TOKEN_CREATION_FAILED, HAL_ED_TOKEN_CREATION_FAILED);
        $errorLDR->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }
    $result = dbCreateSessionToken($db, $clientId, $token);
    if ($result instanceof ErrorHAL)
    {
        $result->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the session token
    finishService($db, ['token' => $token], $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// createTable()
//   Creates an HAL database table and returns SUCCESS as a JSON encoded value
//     OR
//   Returns a JSON encoded HAL error if there is an error
//
function createTable($name)
{
    global $argFilterMap;

    if (DB_DISABLE_TABLE_OPERATIONS)
    {
        // Database operations are disabled
        $errorHAL = new ErrorHAL(HAL_EC_TABLE_OPERATIONS_DISABLED, 'Table creation is disabled');
        $errorHAL->returnErrorJSON(__FUNCTION__);
        return;
    }

    // Get the database connection
    $db = dbConnect();
    if ($db instanceof ErrorHAL)
    {
        $db->returnErrorJSON(__FUNCTION__);
        return;
    }

    // Validate the table name
    $argName = 'tableName';
    $name = validateArg($db, [$argName => $name], $argName, $argFilterMap[$argName], DCO_TOLOWER);
    if ($name instanceof ErrorHAL)
    {
        $name->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Create the table
    $result = dbCreateTable($db, $name);
    if ($result instanceof ErrorHAL)
    {
        $result->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the result
    echo json_encode(['result' => $result]);

    // Close the database connection
    $db = null;
}

// deleteNotificationRecipient()
//   Deletes a notification recipient and returns SUCCESS as a JSON encoded value
//     OR
//   Returns a JSON encoded HAL error if there is an error
//
function deleteNotificationRecipient($notificationListId)
{
    global $argFilterMap;

    $startTime = microtime(true);

    // Initialize the service
    $args = beginService(TOKEN_REQUIRED, __FUNCTION__);
    if ($args instanceof ErrorHAL)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db) = $args;

    // Validate the notificationListId
    $argName = 'notificationListId';
    $notificationListId = validateArg($db, [$argName => $notificationListId], $argName, $argFilterMap[$argName],
                                      DCO_REQUIRED | DCO_MUST_EXIST);
    if ($notificationListId instanceof ErrorHAL)
    {
        $notificationListId->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Delete the notification recipient
    $result = dbDeleteNotificationRecipient($db, $notificationListId);
    if ($result instanceof ErrorHAL)
    {
        $result->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the result
    finishService($db, ['result' => $result], $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// deleteNotificationRule()
//   Deletes a notification rule and returns SUCCESS as a JSON encoded value
//     OR
//   Returns a JSON encoded HAL error if there is an error
//
function deleteNotificationRule($notificationRuleId)
{
    global $argFilterMap;

    $startTime = microtime(true);

    // Initialize the service
    $args = beginService(TOKEN_REQUIRED, __FUNCTION__);
    if ($args instanceof ErrorHAL)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db) = $args;

    // Validate the notificationRuleId
    $argName = 'notificationRuleId';
    $notificationRuleId = validateArg($db, [$argName => $notificationRuleId], $argName, $argFilterMap[$argName],
                                      DCO_REQUIRED | DCO_MUST_EXIST);
    if ($notificationRuleId instanceof ErrorHAL)
    {
        $notificationRuleId->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Delete the notification rule
    $result = dbDeleteNotificationRule($db, $notificationRuleId);
    if ($result instanceof ErrorHAL)
    {
        $result->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the result
    finishService($db, ['result' => $result], $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// deleteSessionToken()
//   Deletes a session_token record and returns the token status as a JSON encoded value
//     OR
//   Returns a JSON encoded HAL error if there is an error
//
function deleteSessionToken()
{
    $startTime = microtime(true);

    // Get the request headers
    $headers = Slim::getInstance()->request()->headers();

    // Get the token
    $token = null;
    if (array_key_exists('TOKEN', $headers))
        $token = $headers['TOKEN'];

    // Check the token length
    if (strlen($token) != LENGTH_SESSION_TOKEN)
    {
        $errorHAL = new ErrorHAL(HAL_EC_TOKEN_LENGTH_INVALID, HAL_ED_TOKEN_LENGTH_INVALID);
        $errorHAL->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Get the database connection
    $db = dbConnect();
    if ($db instanceof ErrorHAL)
    {
        $db->returnErrorJSON(__FUNCTION__);
        return;
    }

    // Check if the token exists
    $dateTime = dbGetSessionDatetime_By_Token($db, $token);
    if ($dateTime instanceof ErrorHAL)
    {
        $dateTime->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }
    if ($dateTime == null)
    {
        $errorHAL = new ErrorHAL(HAL_EC_TOKEN_NOT_FOUND, HAL_ED_TOKEN_NOT_FOUND);
        $errorHAL->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Delete the token
    $result = dbDeleteSessionToken($db, $token);
    if ($result instanceof ErrorHAL)
    {
        $result->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the result
    finishService($db, ['result' => $result], $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

// dropAllTables()
//   Drops all tables from the HAL database except the session_token table and returns SUCCESS as a JSON encoded value
//     OR
//   Returns a JSON encoded HAL error if there is an error
//
function dropAllTables()
{
    $startTime = microtime(true);

    if (DB_DISABLE_TABLE_OPERATIONS)
    {
        // Database operations are disabled
        $errorHAL = new ErrorHAL(HAL_EC_TABLE_OPERATIONS_DISABLED, 'Table deletion is disabled');
        $errorHAL->returnErrorJSON(__FUNCTION__);
        return;
    }

    // Initialize the service
    $args = beginService(TOKEN_REQUIRED, __FUNCTION__);
    if ($args instanceof ErrorHAL)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db) = $args;

    // Drop all HAL tables
    $result = dbDropAllTables($db);
    if ($result instanceof ErrorHAL)
    {
        $result->returnErrorJSON(__FUNCTION__);
        return;
    }

    // Return the result
    finishService($db, ['result' => $result], $startTime, __FUNCTION__);
}

// dropTable()
//   Drops a table from the HAL database and returns SUCCESS as a JSON encoded value
//     OR
//   Returns a JSON encoded HAL error if there is an error
//
function dropTable($name)
{
    global $argFilterMap;

    $startTime = microtime(true);

    if (DB_DISABLE_TABLE_OPERATIONS)
    {
        // Database operations are disabled
        $errorHAL = new ErrorHAL(HAL_EC_TABLE_OPERATIONS_DISABLED, 'Table deletion is disabled');
        $errorHAL->returnErrorJSON(__FUNCTION__);
        return;
    }

    // Initialize the service
    $args = beginService(TOKEN_REQUIRED, __FUNCTION__);
    if ($args instanceof ErrorHAL)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db) = $args;

    // Validate the table name
    $argName = 'tableName';
    $name = validateArg($db, [$argName => $name], $argName, $argFilterMap[$argName], DCO_TOLOWER);
    if ($name instanceof ErrorHAL)
    {
        $name->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Drop the table
    $result = dbDropTable($db, $name);
    if ($result instanceof ErrorHAL)
    {
        $result->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the result
    finishService($db, ['result' => $result], $startTime, __FUNCTION__);
}

// getDatabaseVersion()
//   Returns the database version as JSON
//     OR
//   Returns a JSON LDR error if there is an error
//
function getDatabaseVersion()
{
    // Log the service call
    $startTime = microtime(true);

    // Initialize the service
    $args = beginService(TOKEN_REQUIRED, __FUNCTION__);
    if ($args instanceof ErrorHAL)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db) = $args;

    // Get the database version
    $databaseVersion = dbGetDatabaseVersion($db);
    if ($databaseVersion instanceof ErrorHAL)
    {
        $databaseVersion->returnErrorJSON(__FUNCTION__);
        return;
    }

    // Return the required version and database version
    $versions = ['requiredVersion' => HAL_DATABASE_VERSION, 'databaseVersion' => $databaseVersion];
    finishService($db, $versions, $startTime, __FUNCTION__);
}

// getLogEntries()
//   Returns a set of log entries as a JSON encoded array
//     OR
//   Returns a JSON encoded HAL error if there is an error
//
function getLogEntries()
{
    $startTime = microtime(true);

    // Initialize the service
    $args = beginService(TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            'startDateTime' => DCO_OPTIONAL,
            'endDateTime' => DCO_OPTIONAL,
            'appType' => DCO_OPTIONAL | DCO_TOUPPER,
            'hostname' => DCO_OPTIONAL | DCO_TOLOWER,
            'severity' => DCO_OPTIONAL,
            'eventType1' => DCO_OPTIONAL,
            'eventType2' => DCO_OPTIONAL,
            'appLogId' => DCO_OPTIONAL,
            'pageOffset' => DCO_OPTIONAL,
            'pageLimit' => DCO_OPTIONAL
        ));
    if ($args instanceof ErrorHAL)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $startDateTime, $endDateTime, $appType, $hostname, $severity, $eventType1, $eventType2, $appLogId,
         $pageOffset, $pageLimit) = $args;

    // Perform range checks
    if (strlen($severity) > 0 && $severity > MAX_SEVERITY)
    {
        $errorHAL = createValueError('severity', $severity);
        $errorHAL->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Check that the endDateTime is not before the startDateTime
    if (strlen($startDateTime) > 0 && strlen($endDateTime) > 0)
    {
        if (!datesInOrder($startDateTime, $endDateTime))
        {
            $errorHAL = new ErrorHAL(HAL_EC_END_DATETIME_BEFORE_START_DATETIME, HAL_ED_END_DATETIME_BEFORE_START_DATETIME);
            $errorHAL->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
    }

    // Get the log entries
    if (strlen($pageOffset) > 0 && strlen($pageLimit) > 0)
    {
        $logEntries = dbGetLogEntries($db, $startDateTime, $endDateTime, $appType, $hostname, $severity, $eventType1,
                                      $eventType2, $appLogId, $pageOffset, $pageLimit);
    }
    else
        $logEntries = dbGetLogEntries($db, $startDateTime, $endDateTime, $appType, $hostname, $severity, $eventType1,
                                      $eventType2, $appLogId);
    if ($logEntries instanceof ErrorHAL)
    {
        $logEntries->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the log entries
    finishService($db, $logEntries, $startTime, __FUNCTION__);
}

// getNotificationList()
//   Returns a list of notification recipients as a JSON encoded array
//     OR
//   Returns a JSON encoded HAL error if there is an error
//
function getNotificationList()
{
    $startTime = microtime(true);

    // Initialize the service
    $args = beginService(TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            'notificationRuleId' => DCO_OPTIONAL | DCO_MUST_EXIST,
            'recipient' => DCO_OPTIONAL | DCO_TOLOWER
        ));
    if ($args instanceof ErrorHAL)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $notificationRuleId, $recipient) = $args;

    // Get the notification list
    $notificationList = dbGetNotificationList($db, $notificationRuleId, $recipient);
    if ($notificationList instanceof ErrorHAL)
    {
        $notificationList->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the list
    finishService($db, $notificationList, $startTime, __FUNCTION__);
}

// getNotificationLogEntries()
//   Returns a set of notification log entries as a JSON encoded array
//     OR
//   Returns a JSON encoded HAL error if there is an error
//
function getNotificationLogEntries()
{
    $startTime = microtime(true);

    // Initialize the service
    $args = beginService(TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            'startDateTime' => DCO_OPTIONAL,
            'endDateTime' => DCO_OPTIONAL,
            'appLogId' => DCO_OPTIONAL,
            'notificationRuleId' => DCO_OPTIONAL,
            'recipient' => DCO_OPTIONAL | DCO_TOLOWER,
            'pageOffset' => DCO_OPTIONAL,
            'pageLimit' => DCO_OPTIONAL
        ));
    if ($args instanceof ErrorHAL)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $startDateTime, $endDateTime, $appLogId, $notificationRuleId, $recipient, $pageOffset, $pageLimit) = $args;

    // Check that the endDateTime is not before the startDateTime
    if (strlen($startDateTime) > 0 && strlen($endDateTime) > 0)
    {
        if (!datesInOrder($startDateTime, $endDateTime))
        {
            $errorHAL = new ErrorHAL(HAL_EC_END_DATETIME_BEFORE_START_DATETIME, HAL_ED_END_DATETIME_BEFORE_START_DATETIME);
            $errorHAL->returnErrorJSON(__FUNCTION__);
            $db = null;
            return;
        }
    }

    // Get the log entries
    if (strlen($pageOffset) > 0 && strlen($pageLimit) > 0)
    {
        $logEntries = dbGetNotificationLogEntries($db, $startDateTime, $endDateTime, $appLogId, $notificationRuleId,
                                                  $recipient, $pageOffset, $pageLimit);
    }
    else
    {
        $logEntries = dbGetNotificationLogEntries($db, $startDateTime, $endDateTime, $appLogId, $notificationRuleId,
                                                  $recipient);
    }
    if ($logEntries instanceof ErrorHAL)
    {
        $logEntries->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the log entries
    finishService($db, $logEntries, $startTime, __FUNCTION__);
}

// getNotificationRules()
//   Returns a set of notification rules as a JSON encoded array
//     OR
//   Returns a JSON encoded HAL error if there is an error
//
function getNotificationRules()
{
    $startTime = microtime(true);

    // Initialize the service
    $args = beginService(TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            'appType' => DCO_OPTIONAL | DCO_TOUPPER,
            'severity' => DCO_OPTIONAL,
            'eventType' => DCO_OPTIONAL
        ));
    if ($args instanceof ErrorHAL)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $appType, $severity, $eventType) = $args;

    // Perform range checks
    if (strlen($severity) > 0 && $severity > MAX_SEVERITY)
    {
        $errorHAL = createValueError('severity', $severity);
        $errorHAL->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Get the notification rules
    $notificationRules = dbGetNotificationRules($db, $appType, $severity, $eventType);
    if ($notificationRules instanceof ErrorHAL)
    {
        $notificationRules->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the log entries
    finishService($db, $notificationRules, $startTime, __FUNCTION__);
}

// getPerformanceStatistics()
//   Returns HAL service performance statistics for a given date range as a JSON encoded list of arrays
//     OR
//   Returns a JSON encoded HAL error if there is an error
//
function getPerformanceStatistics()
{
    // Log the service call
    $startTime = microtime(true);

    // Get the database connection and parameters
    $args = beginService(TOKEN_REQUIRED, __FUNCTION__,
        array
        (
            'startDate' => DCO_REQUIRED,
            'endDate' => DCO_REQUIRED
        ));
    if ($args instanceof ErrorHAL)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db, $startDate, $endDate) = $args;

    // Check that the endDate is not before the startDate
    if (!datesInOrder($startDate, $endDate))
    {
        $errorHAL = new ErrorHAL(HAL_EC_END_DATE_BEFORE_START_DATE, HAL_ED_END_DATE_BEFORE_START_DATE);
        $errorHAL->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Get the service statistics
    $services = dbGetPerformanceStatistics($db, $startDate, $endDate);

    // Return the service statistics
    finishService($db, $services, $startTime, __FUNCTION__);
}

// getTableNames()
//   Returns the HAL table names as a JSON encoded array
//     OR
//   Returns a JSON encoded HAL error if there is an error
//
function getTableNames()
{
    // Get the database connection
    $db = dbConnect();
    if ($db instanceof ErrorHAL)
    {
        $db->returnErrorJSON(__FUNCTION__);
        return;
    }

    // Get the table names
    $tables = dbGetTableNames($db);
    if ($tables instanceof ErrorHAL)
    {
        $tables->returnErrorJSON(__FUNCTION__);
        $db = null;
        return;
    }

    // Return the table names
    echo json_encode($tables);

    // Close the database connection
    $db = null;
}

// getVersion()
//   Returns the HAL code version as a JSON encoded value
//
function getVersion()
{
    // Return the response
    $response = ['Server Software Version' => HAL_SOFTWARE_VERSION,
                 'GitHub Commit Date' => HAL_COMMIT_DATE,
                 'Required Database Version' => HAL_DATABASE_VERSION];
    $response = json_encode($response);
    echo $response;
}

// updateDatabase()
//   Performs the required forward or backward migrations on the database and returns the result
//     OR
//   Returns a JSON encoded LDR error if there is an error
//
function updateDatabase()
{
    // Log the service call
    $startTime = microtime(true);

    // Initialize the service
    $args = beginService(TOKEN_REQUIRED, __FUNCTION__);
    if ($args instanceof ErrorHAL)
    {
        $args->returnErrorJSON(__FUNCTION__);
        return;
    }
    list($db) = $args;

    // Update the database
    $result = dbMigrateDatabase($db);
    if ($result instanceof ErrorHAL)
    {
        $result->returnErrorJSON(__FUNCTION__);
        return;
    }

    // Return the migration result
    finishService($db, ['result' => $result], $startTime, __FUNCTION__, RECORD_PERFORMANCE);
}

//
// Support functions
//
function beginService($tokenStatus, $service, $expectedArgs = null)
{
    global $argFilterMap;

    // Initialize the service request arguments
    $args = [];

    // Get the request data
    $requestData = null;
    if (!is_null($expectedArgs))
    {
        switch ($service)
        {
            case 'getLogEntries':
            case 'getNotificationList':
            case 'getNotificationLogEntries':
            case 'getNotificationRules':
            case 'getPerformanceStatistics':
                $request = Slim::getInstance()->request();
                $requestData = $request->params();
                break;
            default:
                $requestData = loadRequestBodyJsonData();
                if ($requestData instanceof ErrorHAL)
                    return $requestData;
                break;
        }
    }

    // Get a database connection
    $db = dbConnect();
    if ($db instanceof ErrorHAL)
        return $db;

    // Check if a session token is required
    if ($tokenStatus != TOKEN_OPTIONAL)
    {
        // Get the request headers
        $headers = Slim::getInstance()->request()->headers();

        // Get the token
        $token = null;
        if (array_key_exists('TOKEN', $headers))
            $token = $headers['TOKEN'];

        // Validate the token
        $token = validateToken($db, $token);
        if ($token instanceof ErrorHAL)
        {
            $db = null;
            return $token;
        }
    }

    // Return the database connection as the first argument
    $args[] = $db;

    // Validate the expected arguments
    if (is_array($expectedArgs))
    {
        foreach ($expectedArgs as $argName => $argDco)
        {
            // Skip any unknown arguments
            if (!array_key_exists($argName, $argFilterMap))
            {
                $errorLDR = new ErrorHAL(HAL_EC_UNSPECIFIED_ERROR_CODE, $argName . ' is missing from $argFilterMap');
                $errorLDR->logError(__FUNCTION__);
                return $errorLDR;
            }

            // Validate the argument
            $arg = validateArg($db, $requestData, $argName, $argFilterMap[$argName], $argDco);
            if ($arg instanceof ErrorHAL)
            {
                $db = null;
                return $arg;
            }

            // Otherwise, append the parameter to the list
            $args[] = $arg;
        }
    }

    // Return the parameters
    return $args;
}

function dateInRange($startDate, $endDate, $testDate)
{
	// Convert the date strings to UNIX timestamps
	$startTS = strtotime($startDate);
	$endTS = strtotime($endDate);
	$testTS = strtotime($testDate);

	// If any of the conversions failed then return false
	if ($startTS === false || $endTS === false || $testTS === false)
		return false;
	
	// Check if the test date lies within the start and end date range
	if ($testTS < $startTS || $testTS > $endTS)
		return false;
	else
		return true;
}

function datesInOrder($firstDate, $secondDate)
{
    // Convert the date strings to UNIX timestamps
    $firstTS = strtotime($firstDate);
    $secondTS = strtotime($secondDate);

    // If either of the conversions failed then return false
    if ($firstTS === false || $secondTS === false)
        return false;

    // Check if the first date is before or equal to the second date
    if ($firstTS <= $secondTS)
        return true;
    else
        return false;
}

// errorQueryExceptionJSON
//   Logs a database query exception and returns a JSON encoded HAL error
//
function errorQueryExceptionJSON(PDOException $error, $functionName, $sql)
{
	$errorLDR = new ErrorHAL(HAL_EC_QUERY_EXCEPTION, $error->getMessage());
	$errorLDR->logMessage($functionName, 'SQL = |' . $sql . '|');
	$errorLDR->returnErrorJSON($functionName);
}

// finishService()
//   Returns the response as a JSON encoded value
//   Updates the daily performance data and logs the elapsed time
//   Closes the database connection
//
function finishService(&$db, $response, $startTime, $service, $recordPerformance = false)
{
    // Return the response
    echo json_encode($response);

    // Optionally update the daily performance record
    if ($recordPerformance)
    {
        $elapsedTime = microtime(true) - $startTime;
        dbRecordPerformance($db, date("Y-m-d"), $service, $elapsedTime);
    }

    // Close the database connection
    $db = null;
}

function generateIdentifier($length)
{
    $identifierCharSet = '23456789ABCDEFGHJKLMNPQRSTUVWXYZ'; // Excludes 0, 1, I, and O
    $charSetMaxIndex = strlen($identifierCharSet) - 1;

    $identifier = "";
    while ($length-- > 0)
        $identifier .= substr($identifierCharSet, mt_rand(0, $charSetMaxIndex), 1);

    return $identifier;
}

function generatePasswordHash($plaintext, $ciphertext = null)
{
    define('PASS_LENGTH', 37);
    define('SALT_LENGTH', 23);
    define('CACA_BEFORE', 29);
    define('CACA_AFTER',  39);

    if ($ciphertext === null)
    {
        $salt = strtoupper(substr(sha1(uniqid(rand(), true)), 0, SALT_LENGTH));
        $caca_before = strtoupper(substr(sha1(uniqid(rand(), true)), 0, CACA_BEFORE));
        $caca_after = strtoupper(substr(sha1(uniqid(rand(), true)), 0, CACA_AFTER));
    }
    else
    {
        $caca_before = substr($ciphertext, 0, CACA_BEFORE);
        $salt = substr($ciphertext, CACA_BEFORE, SALT_LENGTH);
        $caca_after = substr($ciphertext, -CACA_AFTER);
    }

    $password = strtoupper(substr(sha1($salt . $plaintext), 0, PASS_LENGTH));

    return $caca_before . $salt . $password . $caca_after;
}

// getNonEmptyParameterCount()
//   Returns the count of non-empty parameters
//
function getNonEmptyParameterCount($parameters)
{
    // Initialize the count
    $parameterCount = 0;

    // Count the non-empty parameters
    if (is_array($parameters))
    {
        foreach ($parameters as $parameter)
        {
            if (strlen($parameter) > 0)
                $parameterCount++;
        }
    }

    // Return the count
    return $parameterCount;
}

// loadRequestBodyJsonData()
//   Loads the request body JSON as an associative array
//     OR
//   Returns an ErrorHAL object if the request body is missing or is not valid JSON
//
function loadRequestBodyJsonData()
{
    // Get the request
    $request = Slim::getInstance()->request();

    // Get the request body
    $requestBody = $request->getBody();

    // Check if request body is missing
    if (strlen($requestBody) == 0)
    {
        $errorLDR = new ErrorHAL(HAL_EC_REQUEST_BODY_NOT_PROVIDED, HAL_ED_REQUEST_BODY_NOT_PROVIDED);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    // Decode the request body JSON into associative arrays
    $jsonData = json_decode($requestBody, true);
    if ($jsonData === null)
    {
        $errorLDR = new ErrorHAL(HAL_EC_REQUEST_BODY_DECODE_FAILED, HAL_ED_REQUEST_BODY_DECODE_FAILED . $requestBody);
        $errorLDR->logError(__FUNCTION__);
        return $errorLDR;
    }

    return $jsonData;
}

function logError($logId, $error)
{
    // Generate the error log file name
    $filename = 'Log/' . date("Ymd") . LOG_NAME_ERROR;

	// Log the error
	error_log(date('H:i:s') . ' [' . $logId . '] [' . $_SERVER['REMOTE_ADDR'] . '] ERROR ' . $error . CR, 3, $filename);
}

function sendNotifications($mailSubject, $mailFrom, $mailFromName, $recipients, $mailBody)
{
    $mail = new PHPMailer;

    // Configure the email transmission and authentication
    $mail->isSMTP();
    $mail->Host = SMTP_HOSTNAME;
    $mail->SMTPAuth = true;
    $mail->Username = SMTP_USERNAME;
    $mail->Password = SMTP_PASSWORD;
    $mail->SMTPSecure = 'tls';
    $mail->Port = SMTP_PORT;

    // Configure the email values
    $mail->isHTML(true);
    $mail->CharSet = 'UTF-8';
    $mail->From = $mailFrom;
    $mail->FromName = $mailFromName;
    $mail->Subject = $mailSubject;
    $mail->Body = $mailBody;
    $mail->AltBody = $mailBody; // TODO: CREATE A CORRECT AltBody

    // Configure the recipients
    foreach ($recipients as $recipient)
        $mail->addAddress($recipient);

    // Transmit the email(s)
    if ($mail->send())
        $result = SUCCESS;
    else
        $result = new ErrorHAL(HAL_EC_SEND_NOTIFICATION_FAILED, $mail->ErrorInfo);

    return $result;
}
