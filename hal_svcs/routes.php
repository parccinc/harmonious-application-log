<?php

// GET request routing
$app->get('/database/version', 'getDatabaseVersion');
$app->get('/log-entries','getLogEntries');
$app->get('/notification-list','getNotificationList');
$app->get('/notification-log-entries','getNotificationLogEntries');
$app->get('/notification-rules','getNotificationRules');
$app->get('/performance','getPerformanceStatistics');
$app->get('/tables','getTableNames');
$app->get('/version', 'getVersion');

// POST request routing
$app->post('/log-entry', 'createLogEntry');
$app->post('/notification-recipient/:notificationRuleId', 'createNotificationRecipient');
$app->post('/notification-rule', 'createNotificationRule');
$app->post('/session-token', 'createSessionToken');
$app->post('/table/:name', 'createTable');
$app->post('/tables', 'createAllTables');

// PUT request routing
$app->put('/database', 'updateDatabase');

// DELETE request routing
$app->delete('/notification-recipient/:notificationListId', 'deleteNotificationRecipient');
$app->delete('/notification-rule/:notificationRuleId', 'deleteNotificationRule');
$app->delete('/session-token', 'deleteSessionToken');
$app->delete('/table/:name', 'dropTable');
$app->delete('/tables', 'dropAllTables');
