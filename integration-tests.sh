#!/bin/bash

# HAL integration tests
# Usage bash integration-tests.sh url
# url is URL to HAL server root.

URL=
if [ $# -gt 0 ];then
	for (( i=1; i<=$#; i++ ));do
		IFS='=' read -ra ARR <<< "${@:i:1}"
		case "${ARR[0]}" in
			url)
				URL="${ARR[1]}"
				;;
		esac
	done
fi

if [ -z "$URL" ]; then
	echo HAL root URL is required.
	exit 1
fi


# Check that the HAL server is responding
RESPONSE=$( curl -i -j -k $URL/hal_svcs/version 2>&1 )
STATUS=$( echo "$RESPONSE" | egrep ".+\s200\s.+" )
if [ -z "$STATUS" ];then
	echo HAL server is not responding.
	exit 1
else
	echo HAL server is responding.
fi

# Check that the HAL client is responding
RESPONSE=$( curl -i -j -k $URL/hal_client/index.php 2>&1 )
STATUS=$( echo "$RESPONSE" | egrep ".+\s200\s.+" )
if [ -z "$STATUS" ];then
	echo HAL client is not responding.
	exit 1
else
	SUCCESS=$(echo "$RESPONSE" | grep "<p class=\"status\">STATUS: OKAY</p>")
	if [ -z "$SUCCESS" ];then
		echo HAL client is not responding.
		exit 1
	else
		echo HAL client is responding.
	fi
fi

# Check that the HAL client can communicate with the HAL server
RESPONSE=$( curl -i -j -k $URL/hal_client/get_version.php 2>&1 )
STATUS=$( echo "$RESPONSE" | egrep ".+\s200\s.+" )
if [ -z "$STATUS" ];then
	echo HAL client cannot communicate with the HAL server.
	exit 1
else
	SUCCESS=$(echo "$RESPONSE" | grep "<p class=\"status\">STATUS: OKAY</p>")
	if [ -z "$SUCCESS" ];then
		echo HAL client cannot communicate with the HAL server.
		exit 1
	else
		echo HAL client can communicate with the HAL server.
	fi
fi

# Check that the HAL server can connect to the HAL database
RESPONSE=$( curl -i -j -k $URL/hal_client/get_table_names.php 2>&1 )
STATUS=$( echo "$RESPONSE" | egrep ".+\s200\s.+" )
if [ -z "$STATUS" ];then
	echo HAL server cannot connect to the HAL database.
	exit 1
else
	SUCCESS=$(echo "$RESPONSE" | grep "<p class=\"status\">STATUS: OKAY</p>")
	if [ -z "$SUCCESS" ];then
		echo HAL server cannot connect to the HAL database.
		exit 1
	else
		echo HAL server can connect to the HAL database.
	fi
fi

echo Integration tests passed.
exit 0