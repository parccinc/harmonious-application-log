<?php

// Status values for continuous integration (CI) system
define('CI_STATUS_WORD',	'STATUS');
define('CI_STATUS_OKAY',	'OKAY');
define('CI_STATUS_FAIL',	'FAIL');

// Logging definitions
define('LOG_NAME_ERROR',	'_hal_error.log');

// Session token
define('TOKEN_REQUIRED', 1);	// TODO: Token is required
//define('TOKEN_REQUIRED', 2);    // Token is optional for debugging
define('TOKEN_OPTIONAL', 2);	// Token is optional

// Data checks and operations
define('DCO_OPTIONAL',      0x0);   // Argument is optional
define('DCO_REQUIRED',      0x1);   // Argument is required
define('DCO_TOLOWER',       0x2);   // Argument will be converted to lower-case
define('DCO_TOUPPER',       0x4);   // Argument will be converted to upper-case
define('DCO_MUST_EXIST',    0x8);   // Argument must exist in database

// Data filter types
define('DFT_ALPHA_MIXED',               'A-Za-z');
define('DFT_ALPHA_MIXED_PUNCT_A',       'A-Za-z_');
define('DFT_ALPHANUM_MIXED',            '0-9A-Za-z');
define('DFT_ALPHANUM_MIXED_PUNCT_A',    '0-9A-Za-z_');
define('DFT_ALPHANUM_MIXED_PUNCT_B',    '-0-9A-Za-z');
define('DFT_ALPHANUM_MIXED_PUNCT_C',    '-0-9A-Za-z .()\'\/\&\#\+\!\:\%,');
define('DFT_ALPHANUM_MIXED_PUNCT_D',    '-0-9A-Za-z_.');
define('DFT_ALPHANUM_MIXED_PUNCT_E',    '-0-9A-Za-z_ .()\'\/\&\#\+\!\:\%,;<>={}\\"');
define('DFT_ALPHANUM_UPPER',            '0-9A-Z');
define('DFT_DATE',                      '-0-9');
define('DFT_DATETIME',                  '-0-9 :');
define('DFT_EMAIL_ADDRESS',             '-0-9A-Za-z_.\'\/\&\#\+\!\%@={}|~?\$\^');
define('DFT_HEXADECIMAL_UPPER',         '0-9A-F');
define('DFT_NATURAL_NUMBER',            '0-9');
define('DFT_NONE',                       null);

// Database Column Fixed and Maximum Lengths
define('LENGTH_APP_TYPE',         3);
define('LENGTH_EVENT_TYPE',       4);
define('LENGTH_SESSION_TOKEN',   40);
define('LENGTH_SEVERITY',         1);
define('MAXLEN_EVENT_DESC',     500);
define('MAXLEN_HOSTNAME',       300);
define('MAXLEN_RECIPIENT',      255);

// Miscellaneous Definitions
define('CR', "\n");
define('DEFAULT_PAGE_LIMIT', 500);
define('MAX_SEVERITY', 7);
define('SECONDS_PER_DAY', 86400);
define('SECONDS_PER_HOUR', 3600);
define('SUCCESS', 'success');

//
// Error Codes
//
// Next Available: 3035
define('HAL_EC_UNSPECIFIED_ERROR_CODE', '3001');
define('HAL_EC_QUERY_EXCEPTION', '3002');
define('HAL_EC_USE_DATABASE_FAILED', '3003');
define('HAL_EC_CREDENTIALS_NOT_VALID', '3004');
define('HAL_ED_CREDENTIALS_NOT_VALID', 'Client authentication credentials are not valid');
define('HAL_EC_TOKEN_CREATION_FAILED', '3005');
define('HAL_ED_TOKEN_CREATION_FAILED', 'Creation of the session token failed');
define('HAL_EC_DATABASE_CONNECT_FAILED', '3006');
define('HAL_EC_TABLE_OPERATIONS_DISABLED', '3034');
define('HAL_EC_TOKEN_NOT_PROVIDED', '3007');
define('HAL_EC_TOKEN_NOT_FOUND', '3008');
define('HAL_ED_TOKEN_NOT_FOUND', 'Session token not found');
define('HAL_EC_TOKEN_HAS_EXPIRED', '3009');
define('HAL_ED_TOKEN_HAS_EXPIRED', 'Session token has expired');
define('HAL_EC_REQUEST_BODY_NOT_PROVIDED', '3010');
define('HAL_ED_REQUEST_BODY_NOT_PROVIDED', 'Required request body data not provided');
define('HAL_EC_REQUEST_BODY_DECODE_FAILED', '3011');
define('HAL_ED_REQUEST_BODY_DECODE_FAILED', 'Unable to decode this request body: ');
define('HAL_EC_TOKEN_LENGTH_INVALID', '3012');
define('HAL_ED_TOKEN_LENGTH_INVALID', 'Session token length is incorrect');
define('HAL_EC_TOKEN_INVALID_CHARACTERS', '3013');
define('HAL_ED_TOKEN_INVALID_CHARACTERS', 'This session token has invalid characters: ');
define('HAL_EC_ARG_NOT_PROVIDED', '3014');
define('HAL_EC_DATABASE_SET_ATTRIBUTE_FAILED', '3015');
define('HAL_EC_ARG_INVALID_CHARACTERS', '3016');
define('HAL_EC_DATABASE_TABLE_UNKNOWN_TABLE', '3017');
define('HAL_ED_DATABASE_TABLE_UNKNOWN_TABLE', 'This database table is unknown: ');
define('HAL_EC_ARG_INVALID_VALUE', '3018');
define('HAL_EC_END_DATE_BEFORE_START_DATE', '3019');
define('HAL_ED_END_DATE_BEFORE_START_DATE', 'The endDate is before the startDate');
define('HAL_EC_RULE_PARAMETER_NOT_PROVIDED', '3020');
define('HAL_ED_RULE_PARAMETER_NOT_PROVIDED', 'Notification rule parameter not provided');
define('HAL_EC_DUPLICATE_NOTIFICATION_RULE', '3021');
define('HAL_ED_DUPLICATE_NOTIFICATION_RULE', 'A notification rule already exists for ');
define('HAL_EC_SEND_NOTIFICATION_FAILED', '3022');
define('HAL_EC_DATABASE_RECORD_NOT_FOUND', '3023');
define('HAL_ED_DATABASE_RECORD_NOT_FOUND', 'Database record not found for ');
define('HAL_EC_DUPLICATE_NOTIFICATION_RECIPIENT', '3024');
define('HAL_EC_NOTIFICATION_TEMPLATE_LOAD_FAILED', '3025');
define('HAL_EC_APP_TYPE_UNKNOWN', '3026');
define('HAL_ED_APP_TYPE_UNKNOWN', 'This appType is unknown: ');
define('HAL_EC_RECIPIENT_INVALID_FORMAT', '3027');
define('HAL_EC_DATETIME_INVALID_FORMAT', '3028');
define('HAL_EC_DATE_INVALID_FORMAT', '3029');
define('HAL_EC_TIME_INVALID_FORMAT', '3030');
define('HAL_EC_DATE_INVALID_DATE', '3031');
define('HAL_EC_TIME_INVALID_TIME', '3032');
define('HAL_EC_END_DATETIME_BEFORE_START_DATETIME', '3033');
define('HAL_ED_END_DATETIME_BEFORE_START_DATETIME', 'The endDateTime is before the startDateTime');

//
// CLASS DEFINITIONS
//
class ErrorHAL
{
	private $code;
	private $detail;
    private $logId;

	public function __construct($code, $detail = '')
	{
		$this->code = $code;
		$this->detail = $detail;
        $this->logId = generateIdentifier(4);
    }

    public function getErrorArray()
    {
        $error = [];
        $error['error'] = $this->code;
        $error['logid'] = $this->logId;
        $error['detail'] = $this->detail;
        return $error;
    }

	public function returnErrorJSON($reporter)
	{
		// Return the error to the client
        $error = $this->getErrorArray();
		echo json_encode($error);

		// Log the error
		$this->logError($reporter);
	}	
	
	public function logError($reporter)
	{
		logError($this->logId, $this->code . ' [' . $reporter . '] ' . $this->detail);
	}
	
	public function logMessage($reporter, $message)
	{
        logError($this->logId, $this->code . ' [' . $reporter . '] ' . $message);
	}
}

class Performance
{
    public $serviceCount;
    public $totalElapsedTime;
    public $averageElapsedTime;
    public $maximumElapsedTime;
    public $minimumElapsedTime;

    public function __construct()
    {
        $this->serviceCount = 0;
        $this->totalElapsedTime = 0;
        $this->averageElapsedTime = 0;
        $this->maximumElapsedTime = 0;
        $this->minimumElapsedTime = PHP_INT_MAX;
    }
}
