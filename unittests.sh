#!/bin/bash
# HAL unit test execution
# Run as root
# Usage bash unittests.sh requestscheme=scheme halclientuser=user halclientpwd=password rootdir=dir appdir=dir log=false
# requestscheme is HAL client request scheme (http or https. If not specified, script will prompt for scheme.
# halclientuser is HAL client user.
# halclientpwd is HAL client password.
# rootdir is Apache server application root directory.
# appdir is directory on Apache server where code is deployed.
# log enables this script log file (true or false). Default is false.

USELOG=false
LOGFILE=unittests.sh.log
echo `date` >$LOGFILE
REQUESTSCHEME=
HALCLIENTUSER=
HALCLIENTPWD=
ROOTDIR=
APPDIR=

# Process script parameters
if [ $# -gt 0 ];then
	for (( i=1; i<=$#; i++ ));do
		IFS='=' read -ra ARR <<< "${@:i:1}"
		case "${ARR[0]}" in
			requestscheme)
				REQUESTSCHEME="${ARR[1]}"
				;;
				
			halclientuser)
				HALCLIENTUSER="${ARR[1]}"
				;;
				
			halclientpwd)
				HALCLIENTPWD="${ARR[1]}"
				;;
				
			rootdir)
				ROOTDIR="${ARR[1]}"
				;;
				
			appdir)
				APPDIR="${ARR[1]}"
				;;
				
			log)
				USELOG="${ARR[1]}"
				;;
		esac
	done
fi

log() {
	COLOR=
	if [ $# -gt 1 ];then
		case "$2" in
			error)
				COLOR="31"
				;;

			warn)
				COLOR="33"
				;;
					
			ok)
				COLOR="32"
				;;
		esac
	fi

	local MSG=
	if [ -z "$COLOR" ];then
		MSG="$1"
	else
		MSG="\\E[0;""$COLOR""m""$1""\\E[0m"
	fi

	if [ "$USELOG" = true ]; then
		echo -e "$MSG" | tee -a "$LOGFILE"
	else
		echo -e "$MSG"
	fi
}

# Gets Status from HTTP response.
status() {
	local STATUS=$(echo "$1" | awk '/^  HTTP/{print $2}')
	echo "$STATUS"
}

rm -f cookies.txt
REQUEST="wget --server-response --no-cache --no-check-certificate --save-cookies cookies.txt -qO- ""$REQUESTSCHEME""://localhost/$APPDIR/hal_client/login_user.php --post-data clientName=""$HALCLIENTUSER""&password=""$HALCLIENTPWD"
log "$REQUEST"
RESPONSE=$($REQUEST 2>&1)
log "$RESPONSE"

REQUEST="wget --server-response --no-cache --no-check-certificate --keep-session-cookies --load-cookies cookies.txt -qO- ""$REQUESTSCHEME""://localhost/$APPDIR/hal_client/process_service_list.php?filename=run_unit_tests.json"
log "$REQUEST"
RESPONSE=$($REQUEST 2>&1)
log "$RESPONSE"
STATUS=$(status "$RESPONSE")
if [ "$STATUS" = 200 ];then
	SUCCESS=$(echo "$RESPONSE" | grep "<p class=\"status\">STATUS: OKAY</p>")
	if [ -z "$SUCCESS" ];then
		log "Unit tests failed." error
		exit 1
	else
		log "Unit tests passed." ok
	fi
else
	log "Unit tests failed." error
	exit 1
fi

log "Complete!" ok
exit 0